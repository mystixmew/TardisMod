package net.tardis.api.space.thread;

import net.minecraft.util.Direction;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.api.space.OxygenHelper;

import java.util.ArrayList;
import java.util.List;
/** Custom thread to allow the Tardis Mod Oxygen logic to execute*/
public class OxygenThread extends Thread {

    private int maxTries;
    private IOxygenSealer tile;
    private World world;
    private BlockPos start;
    private int triesLeft = 2700;
    private List<BlockPos> airPos = new ArrayList<>();

    public OxygenThread(World world, BlockPos pos, IOxygenSealer tile, int maxTries) {
        super("TARDIS Mod Oxygen Thread");
        this.tile = tile;
        this.start = pos.toImmutable();
        this.world = world;
        this.maxTries = maxTries;
    }

    @Override
    public void run() {
        this.triesLeft = this.maxTries;
        //Clear all positions
        this.airPos.clear();
        for (Direction dir : Direction.values()) {
        	//Fill all defined positions with "oxygen". 
            this.floodFill(world, start.offset(dir), dir.getOpposite());
        }

        //If it used all tries assume it failed to seal
        if (triesLeft > 0) {
            world.getServer().enqueue(new TickDelayedTask(0, () -> {
                tile.setSealedPositions(this.airPos);
            }));
        } else tile.setSealedPositions(new ArrayList<BlockPos>());
    }

    private void floodFill(World world, BlockPos pos, Direction dir) {

        if (triesLeft <= 0)
            return;

        if (OxygenHelper.canOxygenPass(world, world.getBlockState(pos), pos)) {
            airPos.add(pos);
            --triesLeft;

            for (Direction d : Direction.values()) {

                if (!this.airPos.contains(pos.offset(d)))
                    this.floodFill(world, pos.offset(d), d.getOpposite());
            }
        }
    }
}
