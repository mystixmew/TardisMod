package net.tardis.mod;


import net.tardis.mod.cap.items.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraft.block.DispenserBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.world.ForgeChunkManager;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.api.space.cap.OxygenSealerCapability;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.ChunkLoaderCapability;
import net.tardis.mod.cap.IChunkLoader;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.cap.IMissionCap;
import net.tardis.mod.cap.IRift;
import net.tardis.mod.cap.ISpaceDimProperties;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.cap.LightCapability;
import net.tardis.mod.cap.MissionWorldCapability;
import net.tardis.mod.cap.RiftCapability;
import net.tardis.mod.cap.SpaceDimensionCapability;
import net.tardis.mod.cap.TardisWorldCapability;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.entity.PlayerDataCapability;
import net.tardis.mod.cap.items.sonic.ISonic;
import net.tardis.mod.cap.items.sonic.SonicCapability;
import net.tardis.mod.cap.items.sonic.SonicStorage;
import net.tardis.mod.commands.TardisCommand;
import net.tardis.mod.compat.vanilla.DalekDispenseBehaviour;
import net.tardis.mod.compat.vanilla.EntitySpawnerDispenseBehaviour;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.containers.TContainers;
import net.tardis.mod.entity.BessieEntity;
import net.tardis.mod.entity.HoloPilotEntity;
import net.tardis.mod.entity.SecDroidEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.TDataSerializers;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;
import net.tardis.mod.entity.humanoid.CompanionEntity;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.events.CommonEvents;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.TardisLike;
import net.tardis.mod.missions.stages.MissionStages;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ARSPieceSyncMessage;
import net.tardis.mod.network.packets.ConsoleRoomSyncMessage;
import net.tardis.mod.network.packets.SchematicSyncMessage;
import net.tardis.mod.network.packets.TardisLikeSyncMessage;
import net.tardis.mod.particles.TParticleTypes;
import net.tardis.mod.potions.TardisPotions;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.registries.BrokenExteriors;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.DalekTypeRegistry;
import net.tardis.mod.registries.DisguiseRegistry;
import net.tardis.mod.registries.ExteriorAnimationRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.registries.FlightEventRegistry;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.registries.MissionRegistry;
import net.tardis.mod.registries.ProtocolRegistry;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.registries.SonicModeRegistry;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.registries.TardisStatistics;
import net.tardis.mod.registries.TraitRegistry;
import net.tardis.mod.registries.UpgradeRegistry;
import net.tardis.mod.registries.VortexMFunctionCategories;
import net.tardis.mod.registries.VortexMFunctions;
import net.tardis.mod.schematics.SchematicDataListener;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ChunkLoaderTile.ChunkValidationCallback;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.trades.TPointOfInterest;
import net.tardis.mod.trades.TVillagerProfession;
import net.tardis.mod.world.WorldGen;
import net.tardis.mod.world.biomes.TBiomes;
import net.tardis.mod.world.dimensions.TDimensions;
import net.tardis.mod.world.feature.TFeatures;
import net.tardis.mod.world.structures.TStructures;
import net.tardis.mod.world.surfacebuilders.TSurfaceBuilders;

@Mod(Tardis.MODID)
public class Tardis {
    public static final String MODID = "tardis";

    public static Logger LOGGER = LogManager.getLogger(MODID);

    public static final ResourceLocation TARDIS_LOC = new ResourceLocation(Tardis.MODID, "tardis");

    public Tardis() {
        IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();
        modBus.addListener(this::commonSetup);
        modBus.addListener(this::registerEntityAttributes);
        MinecraftForge.EVENT_BUS.register(this);

        //Register our Deferred Registries here so it can be registered early enough to avoid NPEs
    	TBlocks.BLOCKS.register(modBus);
    	ConsoleRegistry.CONSOLES.register(modBus);
    	DisguiseRegistry.DISGUISES.register(modBus);
    	TTiles.TILES.register(modBus);
    	TEntities.ENTITIES.register(modBus);
    	TItems.ITEMS.register(modBus);
    	TContainers.CONTAINERS.register(modBus);
    	TSounds.SOUNDS.register(modBus);
    	TardisPotions.EFFECTS.register(modBus);
    	SoundSchemeRegistry.SOUND_SCHEMES.register(modBus);
    	VortexMFunctionCategories.FUNCTION_CATEGORIES.register(modBus);
        SchematicTypes.SCHEMATIC_TYPES.register(modBus);
    	
    	TSurfaceBuilders.SurfaceBuilders.SURFACE_BUILDERS.register(modBus);
    	TBiomes.BIOMES.register(modBus);
    	TStructures.Structures.STRUCTURES.register(modBus);
    	TFeatures.FEATURES.register(modBus);
    	TPointOfInterest.POINT_OF_INTERESTS.register(modBus);
    	TVillagerProfession.VILLAGE_PROFFESIONS.register(modBus);
        
    	MissionStages.STAGES.register(modBus);
    	MissionRegistry.MISSIONS.register(modBus);
    	FlightEventRegistry.FLIGHT_EVENTS.register(modBus);
    	TraitRegistry.TRAITS.register(modBus);
    	ProtocolRegistry.PROTOCOLS.register(modBus);
    	ControlRegistry.CONTROLS.register(modBus);
    	UpgradeRegistry.UPGRADES.register(modBus);
    	ExteriorRegistry.EXTERIORS.register(modBus);
    	ExteriorAnimationRegistry.EXTERIOR_ANIMATIONS.register(modBus);
    	InteriorHumRegistry.HUMS.register(modBus);
    	TParticleTypes.TYPES.register(modBus);
    	DalekTypeRegistry.DALEK_TYPES.register(modBus);
    	SonicModeRegistry.SONIC_MODES.register(modBus);
    	VortexMFunctions.FUNCTIONS.register(modBus);
    	BrokenExteriors.BROKEN_EXTERIOR_TYPES.register(modBus);

    	TardisRecipeSerialisers.RECIPE_SERIALISERS.register(modBus);

    	
        TTriggers.init();
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, TConfig.COMMON_SPEC);
        ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, TConfig.CLIENT_SPEC);
        ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, TConfig.SERVER_SPEC);
        
        Network.init();
        ConsoleRoom.DATA_LOADER.subscribeAsSyncable(Network.getNetworkChannel(), ConsoleRoomSyncMessage::new);
        ARSPiece.DATA_LOADER.subscribeAsSyncable(Network.getNetworkChannel(), ARSPieceSyncMessage::new);
        SchematicDataListener.INSTANCE.subscribeAsSyncable(Network.getNetworkChannel(), SchematicSyncMessage::new);
        TardisLike.DATA_LOADER.subscribeAsSyncable(Network.getNetworkChannel(), TardisLikeSyncMessage::new);
//        WeldRecipe.DATA_LOADER.subscribeAsSyncable(Network.getNetworkChannel(), WeldRecipeSyncMessage::new);
//        AlembicRecipe.DATA_LOADER.subscribeAsSyncable(Network.getNetworkChannel(), AlembicRecipeSyncMessage::new);
    }

    private void commonSetup(FMLCommonSetupEvent event) {

        event.enqueueWork(() ->
        {
            TFeatures.registerConfiguredFeatures();
            TStructures.setupStructures();
            TStructures.ConfiguredStructures.registerConfiguredStructures();
            TBiomes.registerBiomeKeys();
            TDimensions.registerNoiseSettings();
            TDimensions.registerChunkGenerators();
            WorldGen.addEntitiesToRaids();
            VortexMFunctions.addFunctionToCategories();
            ForgeChunkManager.setForcedChunkLoadingCallback(MODID, ChunkValidationCallback.INSTANCE);
            TardisCommand.registerCustomArgumentTypes();
            TardisStatistics.addStatTypesToVanilla();
            registerDispenserBehaviours();
        });
        
        CapabilityManager.INSTANCE.register(ILightCap.class, new ILightCap.LightStorage(), LightCapability::new);
//        CapabilityManager.INSTANCE.register(IChunkLoader.class, new ChunkLoaderCapability.LoaderStorage(), () -> new ChunkLoaderCapability(null));
        CapabilityManager.INSTANCE.register(ITardisWorldData.class, new ITardisWorldData.TardisWorldStorage(), () -> new TardisWorldCapability(null));
        CapabilityManager.INSTANCE.register(IRift.class, new IRift.Storage(), () -> new RiftCapability(null));
        CapabilityManager.INSTANCE.register(IMissionCap.class, new IMissionCap.Storage(), () -> new MissionWorldCapability(null));
        CapabilityManager.INSTANCE.register(ISpaceDimProperties.class, new ISpaceDimProperties.Storage(), () -> new SpaceDimensionCapability(null));
        CapabilityManager.INSTANCE.register(IOxygenSealer.class, new IOxygenSealer.Storage(), () -> new OxygenSealerCapability(100));

        //ItemCaps
        CapabilityManager.INSTANCE.register(IVortexCap.class, new IVortexCap.Storage(), VortexCapability::new);
        CapabilityManager.INSTANCE.register(ISonic.class, new SonicStorage(), SonicCapability::new);
        CapabilityManager.INSTANCE.register(IWatch.class, new IWatch.Storage(), WatchCapability::new);
        CapabilityManager.INSTANCE.register(IRemote.class, new IRemote.Storage(), () -> new RemoteCapability(null));
        CapabilityManager.INSTANCE.register(IDiagnostic.class, new IDiagnostic.Storage(), () -> new DiagnosticToolCapability(null));

        //Entity Caps
        CapabilityManager.INSTANCE.register(IPlayerData.class, new IPlayerData.Storage(), () -> new PlayerDataCapability(null));

        TDataSerializers.register();
        CommonEvents.getAllMappingEntries();
        FlightEventRegistry.registerRandomEntries();
    }
    
    public void registerEntityAttributes(EntityAttributeCreationEvent event) {
    	event.put(TEntities.DALEK.get(), DalekEntity.createAttributes().create());
    	event.put(TEntities.SECURITY_DROID.get(), SecDroidEntity.createAttributes().create());
    	event.put(TEntities.BESSIE.get(), BessieEntity.createAttributes().create());
    	event.put(TEntities.COMPANION.get(), CompanionEntity.createAttributes().create());
    	event.put(TEntities.SHIP_CAPTAIN.get(), ShipCaptainEntity.createAttributes().create());
    	event.put(TEntities.CREWMATE.get(), CrewmateEntity.createAttributes().create());
    	event.put(TEntities.HOLO_PILOT.get(), HoloPilotEntity.createAttributes().create());
    }
    
    private void registerDispenserBehaviours() {
    	DispenserBlock.registerDispenseBehavior(TItems.SPAWN_EGG_DALEK_DEFAULT.get(), new DalekDispenseBehaviour(DalekTypeRegistry.DEFAULT.get()));
    	DispenserBlock.registerDispenseBehavior(TItems.SPAWN_EGG_DALEK_RUSTY.get(), new DalekDispenseBehaviour(DalekTypeRegistry.RUSTY.get()));
    	DispenserBlock.registerDispenseBehavior(TItems.SPAWN_EGG_DALEK_SPECIAL_WEAPONS.get(), new DalekDispenseBehaviour(DalekTypeRegistry.SPECIAL.get()));
    	
    	DispenserBlock.registerDispenseBehavior(TItems.INT_DOOR.get(), new EntitySpawnerDispenseBehaviour(TEntities.DOOR.get(), -0.5D, 0.5D));
    }

}
