package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.helper.VoxelShapeUtils;

public class ChemLightBlock extends Block implements IARS{

    public final VoxelShape UP = VoxelShapes.create(0.4296875, 0.0, 0.4296875, 0.5703125, 0.75, 0.5703125);
    public final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Direction.WEST);
    public final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Direction.NORTH);
    public final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Direction.EAST);
    public final VoxelShape NORTH = VoxelShapeUtils.rotate(createVoxelShape(), Direction.SOUTH);
    public final VoxelShape DOWN = VoxelShapes.create(0.4296875, 0.25, 0.4296875, 0.5703125, 1.0, 0.5703125);

    public ChemLightBlock(Properties properties) {
        super(properties);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        Direction direction = context.getFace();
        BlockState blockstate = context.getWorld().getBlockState(context.getPos().offset(direction.getOpposite()));
        return blockstate.getBlock() == this && blockstate.get(BlockStateProperties.FACING) == direction ? this.getDefaultState().with(BlockStateProperties.FACING, direction.getOpposite()) : this.getDefaultState().with(BlockStateProperties.FACING, direction);
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.FACING);
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.with(BlockStateProperties.FACING, mirrorIn.mirror(state.get(BlockStateProperties.FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(BlockStateProperties.FACING, rot.rotate(state.get(BlockStateProperties.FACING)));
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.FACING)) {
            case DOWN:
                return DOWN;
            case EAST:
                return EAST;
            case NORTH:
                return NORTH;
            case SOUTH:
                return SOUTH;
            case UP:
                return UP;
            case WEST:
                return WEST;
            default:
                return UP;
        }

    }

    public VoxelShape createVoxelShape() {
        return VoxelShapes.create(0.4296875, 0.0, 0.4296875, 0.5703125, 0.75, 0.5703125);
    }

}
