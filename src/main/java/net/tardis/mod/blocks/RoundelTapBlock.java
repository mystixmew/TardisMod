package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.properties.Prop;

public class RoundelTapBlock extends NotSolidTileBlock {
    public static final BooleanProperty PUSH = BooleanProperty.create("push");
    public static final TranslationTextComponent PUSH_TO_TARDIS = new TranslationTextComponent("message.tardis.roundel_tap.push_to_tardis");
    public static final TranslationTextComponent PULL_TO_BLOCKS_FROM_TARDIS = new TranslationTextComponent("message.tardis.roundel_tap.push_to_blocks");
    public static final VoxelShape NORTH = VoxelShapeUtils.rotate(createVoxelShape(), Direction.SOUTH);
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Direction.WEST);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Direction.NORTH);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Direction.EAST);
    public static final VoxelShape UP = createVoxelShape();
    public static final VoxelShape DOWN = VoxelShapeUtils.rotate(createVoxelShape(), Direction.UP);

    private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.roundel_tap.purpose"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.roundel_tap.use"));
    
    
    public RoundelTapBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    public static VoxelShape createVoxelShape() {
    	VoxelShape shape = VoxelShapes.create(0.0, 0.0, 0.0, 1.0, 0.0625, 1.0);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.125, 0.046875, 0.125, 0.875, 0.796875, 0.875), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.1875, 0.78125, 0.1875, 0.8125, 0.84375, 0.8125), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.25, 0.0625, 0.25, 0.75, 0.8125, 0.75), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.8125, 0.375, 0.625, 1.0, 0.625), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0, 0.375, 0.375, 0.125, 0.625, 0.625), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.375, 0.875, 0.625, 0.625, 1.0), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.875, 0.375, 0.375, 1.0, 0.625, 0.625), IBooleanFunction.OR);
    	shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.375, 0.375, 0.0, 0.625, 0.625, 0.125), IBooleanFunction.OR);
    	return shape;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
    	switch (state.get(BlockStateProperties.FACING)) {
            case NORTH:
                return NORTH;
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case WEST:
            	return WEST;
            case UP:
                return UP;
            case DOWN:
                return DOWN;
            default:
            	return createVoxelShape();
        }
    }

    @Override
    public void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.FACING).add(PUSH);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return super.getStateForPlacement(context).with(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
        if (Screen.hasControlDown()) {
            tooltip.clear();
            tooltip.add(0, stack.getDisplayName());
            tooltip.add(TardisConstants.Translations.NO_USE_OUTSIDE_TARDIS);
            tooltip.add(descriptionTooltip);
            tooltip.add(descriptionTooltipTwo);
        }
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
        if (placer instanceof PlayerEntity) { //Warn the player that it only works in the tardis
            if (WorldHelper.isDimensionBlocked(worldIn))
                PlayerHelper.sendMessageToPlayer((PlayerEntity) placer, TardisConstants.Translations.NO_USE_OUTSIDE_TARDIS, true);
        }
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player,
            Hand hand, BlockRayTraceResult hit) {
        if (!world.isRemote()) {
            BlockState newState = WorldHelper.cycleBlockStateProperty(state, PUSH);
            world.setBlockState(pos, newState, 3);
            boolean isPushingToTardis = newState.get(PUSH);
            player.sendStatusMessage(!isPushingToTardis ? PUSH_TO_TARDIS : PULL_TO_BLOCKS_FROM_TARDIS, true);
        }
        return ActionResultType.SUCCESS;
    }
    
}
