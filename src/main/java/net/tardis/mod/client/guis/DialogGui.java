package net.tardis.mod.client.guis;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextProperties;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;

public class DialogGui extends Screen {

    Dialog dialog;
    AbstractHumanoidEntity speaker;

    public DialogGui(AbstractHumanoidEntity speaker) {
        super(TardisConstants.Gui.DEFAULT_GUI_TITLE);
        this.speaker = speaker;
    }

    @Override
    protected void init() {
        super.init();
        this.dialog = this.speaker.getCurrentDialog(Minecraft.getInstance().player);

        this.setupButtons();
    }

    public void setupButtons() {

        //Close if dialog is null
        if (this.dialog == null) {
            Minecraft.getInstance().displayGuiScreen((Screen)null);
            return;
        }

        //Clean up old buttons and clear them
        for (Widget w : this.buttons)
            w.active = false;
        this.buttons.clear();

        //Setup dialog options
        int i = 0;
        for (DialogOption option : this.dialog.getOptions()) {
            this.addButton(new TextButton(this.width / 2 - 130, this.height / 2 + i * (this.font.FONT_HEIGHT + 4), option.getTranslation().getString(), but -> {
                this.dialog = option.onClick(this.speaker, Minecraft.getInstance().player);
                this.setupButtons();
            }));
            ++i;
        }
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

    @Override
    public void render(MatrixStack matrixStack, int x, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        if (this.speaker != null) {
            IFormattableTextComponent displayName = new StringTextComponent(this.speaker.getDisplayName().getString()).mergeStyle(TextFormatting.GOLD);
            drawString(matrixStack, font, displayName, this.width / 2 - 150, this.height / 2 - 100, 0xFFFFFF);
        }
        PlayerEntity player = Minecraft.getInstance().player;
        if (player != null) {
        	IFormattableTextComponent playerName = new StringTextComponent(player.getDisplayName().getString()).mergeStyle(TextFormatting.AQUA);
        	drawString(matrixStack, font, playerName, this.width / 2 - 150, this.height / 2 - 20, 0xFFFFFF);
        }
        if (this.dialog != null) {
        	TranslationTextComponent text = this.dialog.getSpeakerLine();
            int i = 0;
            int maxWidth = (int) (Minecraft.getInstance().getMainWindow().getScaledWidth() * 0.6);
            for (ITextProperties s : this.font.getCharacterManager().func_238362_b_(text, maxWidth, Style.EMPTY)) {
            	StringTextComponent st = new StringTextComponent(s.getString());
            	drawString(matrixStack, font, st, this.width / 2 - 130, this.height / 2 - 75 + ((font.FONT_HEIGHT * i) - 3), 0xFFFFFF);
                ++i;
            }

        }

        super.render(matrixStack, x, mouseY, partialTicks);
    }
}
