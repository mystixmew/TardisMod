package net.tardis.mod.client.guis.ars;

import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.ars.ARSPieceCategory;
import net.tardis.mod.client.guis.ARSTabletScreen;
import net.tardis.mod.client.guis.widgets.TextButton;

public class ARSCategoryButton extends TextButton {

    private ARSTabletScreen parent;
    private ARSPieceCategory category;

    public ARSCategoryButton(int x, int y, ARSPieceCategory cat, ARSTabletScreen parent) {
        super(x, y, (TranslationTextComponent) cat.getTranslation().mergeStyle(TextFormatting.BOLD), null);
        this.parent = parent;
        this.category = cat;
    }

    @Override
    public void onPress() {
        parent.openCategory(this.category);
    }



}
