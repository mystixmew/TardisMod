package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.guis.widgets.IntSliderWidget;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleVariantMessage;
import net.tardis.mod.network.packets.LightUpdateMessage;
import net.tardis.mod.network.packets.UpdateGravityMessage;
import net.tardis.mod.registries.DisguiseRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class InteriorEditScreen extends MonitorScreen {

    private final ConsoleTile tile;
    private final TranslationTextComponent lightLevelText = new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior_properties.interior_light_level");
    private final TranslationTextComponent console_variant = new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior_properties.console_variant");
    public IntSliderWidget lightSlider;
    public IntSliderWidget gravSlider;
    private TextButton texVarButton;
    private int texIndex = 0;
    private TexVariant selected;

    public InteriorEditScreen(IMonitorGui gui, String submenu) {
        super(gui, submenu);

        tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
        if (tile != null) {
            int index = 0;
            for (TexVariant v : tile.getTextureVariants()) {
                if (v == tile.getVariant())
                    break;
                ++index;
            }
            this.texIndex = index;
        }
    }

    @Override
    public void init(Minecraft p_init_1_, int p_init_2_, int p_init_3_) {
        super.init(p_init_1_, p_init_2_, p_init_3_);
        if (parent instanceof Screen) {
            ((Screen) parent).init(p_init_1_, p_init_2_, p_init_3_);
        }
    }

    @Override
    protected void init() {
        super.init();
        //this.buttons.clear();
        double sliderVal = 0.1;
        TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
        if (te instanceof ConsoleTile) {
            sliderVal = ((ConsoleTile) te).getInteriorManager().getLight() / 15.0;

        }
        this.addButton(lightSlider = new IntSliderWidget(this.parent.getMinX(), this.parent.getMinY() - 10, 100, 20, new TranslationTextComponent("gui.tardis.protocol.interior_properties.interior_light_level.slider"), sliderVal, slider -> Network.sendToServer(new LightUpdateMessage((float)slider))));


        this.addButton(gravSlider = new IntSliderWidget(this.parent.getMinX(), this.parent.getMinY() - 30, 100, 20, new TranslationTextComponent("gui.tardis.protocol.interior_properties.grav.slider"), 1.0, slider -> {
            Network.sendToServer(new UpdateGravityMessage((float)slider));
        }));

        this.addButton(new TextButton(this.parent.getMinX(), this.parent.getMinY() - this.getUsedHeight() - 30, "> " + new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior_properties.hum")
                .getString(), but -> Minecraft.getInstance().displayGuiScreen(new InteriorHumsScreen(this.parent, "interior_hum"))));

        if (this.tile.getTextureVariants().length > 0) {

            this.modTexture(0);


            this.addButton(texVarButton = new TextButton(this.parent.getMinX(), this.parent.getMinY() - 40,
                    console_variant.getString() + this.selected.getTranslation().getString(),
                    but -> modTexture(1)));
        }

        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior_properties.sound_scheme"), but -> {
            ClientHelper.openGui(new SoundSchemeMonitorScreen(this.parent, "sound_scheme"));
        });

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        for (Widget w : this.buttons)
            w.renderWidget(matrixStack, mouseX, mouseY, partialTicks);

        drawString(matrixStack, this.minecraft.fontRenderer, lightLevelText.getUnformattedComponentText(),
                this.parent.getMinX(), this.parent.getMinY() - 20, 0xFFFFFF);
    }

    @Override
    public int getUsedHeight() {
        return 72;
    }

    public void modTexture(int i) {
        if (this.texIndex + i >= this.tile.getTextureVariants().length)
            this.texIndex = 0;
        else if (this.texIndex + i < 0)
            this.texIndex = tile.getTextureVariants().length - 1;
        else this.texIndex += i;

        this.selected = tile.getTextureVariants()[this.texIndex];
        if (this.texVarButton != null)
            this.texVarButton.setMessage(new TranslationTextComponent(console_variant.getString() + this.selected.getTranslation().getString()));

        if (1 != 0)
            Network.sendToServer(new ConsoleVariantMessage(this.texIndex));
    }

}
