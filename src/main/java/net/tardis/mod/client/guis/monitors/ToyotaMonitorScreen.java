package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.sounds.TSounds;

public class ToyotaMonitorScreen extends BaseMonitorScreen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/toyota.png");

    @Override
    public void renderMonitor(MatrixStack matrixStack) {
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        this.renderBackground(matrixStack);
        int texWidth = 256, texHeight = 185;
        this.blit(matrixStack, width / 2 - texWidth / 2, height / 2 - texHeight / 2, 0, 0, texWidth, texHeight);

    }

    @Override
    protected void init() {
        super.init();
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.STEAMPUNK_MONITOR_INTERACT.get(), 1.0F));
    }

    @Override
    public int getMinY() {
        return this.height / 2 + 32;
    }

    @Override
    public int getMinX() {
        return this.width / 2 - 115;
    }

    @Override
    public int getMaxX() {
        return this.getMinX() + 245;
    }

    @Override
    public int getMaxY() {
        return this.getMinY() - 110;
    }

    @Override
    public int getWidth() {
        return 201;
    }

    @Override
    public int getHeight() {
        return 152;
    }

    @Override
    public int getGuiID() {
        return TardisConstants.Gui.MONITOR_MAIN_TOYOTA;
    }


}
