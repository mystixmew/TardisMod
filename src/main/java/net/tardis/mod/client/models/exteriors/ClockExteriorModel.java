package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.IBrokenExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ClockExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.WorldHelper;
//import net.tardis.mod.enums.EnumDoorState;
//import net.tardis.mod.enums.EnumMatterState;
//import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;


public class ClockExteriorModel extends ExteriorModel implements IBrokenExteriorModel{
    private final LightModelRenderer glow_clockface;
    private final ModelRenderer boti;
    private final ModelRenderer door_rotate_y;
    private final ModelRenderer pendulum_rotate_z;
    private final ModelRenderer disk;
    private final ModelRenderer pendulum_backwall;
    private final ModelRenderer pully;
    private final ModelRenderer pully2;
    private final ModelRenderer door_frame;
    private final ModelRenderer door_latch;
    private final ModelRenderer grandfather_clock;
    private final ModelRenderer top;
    private final ModelRenderer posts;
    private final ModelRenderer foot1;
    private final ModelRenderer foot2;
    private final ModelRenderer foot3;
    private final ModelRenderer foot4;
    private final ModelRenderer torso;
    private final ModelRenderer clockface;
    private final ModelRenderer numbers;
    private final ModelRenderer twelveToSix;
    private final ModelRenderer fiveTo11;
    private final ModelRenderer fourTo10;
    private final ModelRenderer threeToNine;
    private final ModelRenderer twoToEight;
    private final ModelRenderer oneToSeven;
    private final ModelRenderer hand_rotate_z;
    private final ModelRenderer side_details;
    private final ModelRenderer side_details2;
    private final ModelRenderer back_details;
    private final ModelRenderer base;

    public ClockExteriorModel() {
        textureWidth = 512;
        textureHeight = 512;

        glow_clockface = new LightModelRenderer(this);
        glow_clockface.setRotationPoint(0.0F, 24.0F, 0.0F);
        glow_clockface.setTextureOffset(230, 180).addBox(-20.0F, -167.0F, 0.0F, 40.0F, 48.0F, 6.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(412, 194).addBox(-20.0F, -121.0F, 4.0F, 40.0F, 100.0F, 4.0F, 0.0F, false);

        door_rotate_y = new ModelRenderer(this);
        door_rotate_y.setRotationPoint(20.0F, -4.0F, -5.0F);
        

        pendulum_rotate_z = new ModelRenderer(this);
        pendulum_rotate_z.setRotationPoint(-20.0F, -88.0F, 5.0F);
        door_rotate_y.addChild(pendulum_rotate_z);
        pendulum_rotate_z.setTextureOffset(194, 185).addBox(-2.0F, 7.0F, 0.0F, 4.0F, 56.0F, 1.0F, 0.0F, false);

        disk = new ModelRenderer(this);
        disk.setRotationPoint(0.0F, 69.0F, 0.0F);
        pendulum_rotate_z.addChild(disk);
        setRotationAngle(disk, 0.0F, 0.0F, -0.7854F);
        disk.setTextureOffset(11, 184).addBox(-6.0F, -6.0F, -1.0F, 12.0F, 12.0F, 2.0F, 0.0F, false);

        pendulum_backwall = new ModelRenderer(this);
        pendulum_backwall.setRotationPoint(-20.0F, 28.0F, 5.0F);
        door_rotate_y.addChild(pendulum_backwall);
        pendulum_backwall.setTextureOffset(75, 190).addBox(-16.0F, -109.0F, 2.0F, 32.0F, 84.0F, 0.0F, 0.0F, false);

        pully = new ModelRenderer(this);
        pully.setRotationPoint(-4.0F, 0.0F, 5.0F);
        door_rotate_y.addChild(pully);
        pully.setTextureOffset(198, 195).addBox(-24.0F, -81.0F, 0.0F, 2.0F, 12.0F, 1.0F, 0.0F, false);
        pully.setTextureOffset(19, 192).addBox(-25.0F, -69.0F, -1.0F, 4.0F, 12.0F, 2.0F, 0.0F, false);

        pully2 = new ModelRenderer(this);
        pully2.setRotationPoint(-4.0F, 0.0F, 5.0F);
        door_rotate_y.addChild(pully2);
        pully2.setTextureOffset(198, 196).addBox(-10.0F, -81.0F, 0.0F, 2.0F, 24.0F, 1.0F, 0.0F, false);
        pully2.setTextureOffset(17, 188).addBox(-11.0F, -57.0F, -1.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

        door_frame = new ModelRenderer(this);
        door_frame.setRotationPoint(-20.0F, 28.0F, 5.0F);
        door_rotate_y.addChild(door_frame);
        door_frame.setTextureOffset(149, 90).addBox(-16.0F, -105.0F, -4.0F, 4.0F, 72.0F, 3.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(8.0F, -161.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-16.0F, -161.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-20.0F, -157.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(12.0F, -157.0F, -5.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-16.0F, -165.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-16.0F, -121.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-16.0F, -113.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(107, 164).addBox(-16.0F, -29.0F, -5.0F, 32.0F, 4.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(101, 36).addBox(-20.0F, -153.0F, -5.0F, 4.0F, 128.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(120, 36).addBox(16.0F, -153.0F, -5.0F, 4.0F, 128.0F, 8.0F, 0.0F, false);
        door_frame.setTextureOffset(89, 77).addBox(-16.0F, -117.0F, -4.0F, 32.0F, 4.0F, 0.0F, 0.0F, false);
        door_frame.setTextureOffset(143, 49).addBox(-16.0F, -117.0F, 1.0F, 32.0F, 4.0F, 1.0F, 0.0F, false);
        door_frame.setTextureOffset(121, 103).addBox(-16.0F, -109.0F, -4.0F, 32.0F, 4.0F, 3.0F, 0.0F, false);
        door_frame.setTextureOffset(137, 84).addBox(12.0F, -105.0F, -4.0F, 4.0F, 72.0F, 3.0F, 0.0F, false);
        door_frame.setTextureOffset(111, 164).addBox(-16.0F, -33.0F, -4.0F, 32.0F, 4.0F, 3.0F, 0.0F, false);

        door_latch = new ModelRenderer(this);
        door_latch.setRotationPoint(-20.0F, 28.0F, 5.0F);
        door_rotate_y.addChild(door_latch);
        door_latch.setTextureOffset(11, 183).addBox(-19.0F, -97.0F, -7.0F, 2.0F, 8.0F, 12.0F, 0.0F, false);

        grandfather_clock = new ModelRenderer(this);
        grandfather_clock.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        top = new ModelRenderer(this);
        top.setRotationPoint(0.0F, 0.0F, 0.0F);
        grandfather_clock.addChild(top);
        top.setTextureOffset(80, 71).addBox(16.0F, -165.0F, -8.0F, 12.0F, 8.0F, 32.0F, 0.0F, false);
        top.setTextureOffset(33, 60).addBox(-28.0F, -165.0F, -8.0F, 12.0F, 8.0F, 32.0F, 0.0F, false);
        top.setTextureOffset(34, 49).addBox(-32.0F, -169.0F, -9.0F, 64.0F, 4.0F, 34.0F, 0.0F, false);
        top.setTextureOffset(33, 60).addBox(-33.0F, -171.0F, -10.0F, 66.0F, 4.0F, 36.0F, 0.0F, false);
        top.setTextureOffset(33, 60).addBox(-31.0F, -173.0F, -8.0F, 62.0F, 4.0F, 32.0F, 0.0F, false);

        posts = new ModelRenderer(this);
        posts.setRotationPoint(0.0F, 0.0F, 0.0F);
        grandfather_clock.addChild(posts);
        

        foot1 = new ModelRenderer(this);
        foot1.setRotationPoint(0.0F, 0.0F, 0.0F);
        posts.addChild(foot1);
        foot1.setTextureOffset(16, 159).addBox(20.0F, -5.0F, -9.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        foot1.setTextureOffset(16, 159).addBox(21.0F, -2.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(16, 159).addBox(21.0F, -7.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(111, 36).addBox(22.0F, -158.0F, -7.0F, 4.0F, 134.0F, 4.0F, 0.0F, false);
        foot1.setTextureOffset(16, 159).addBox(21.0F, -31.0F, -8.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(16, 159).addBox(21.0F, -33.6F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(16, 159).addBox(21.0F, -36.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(129, 46).addBox(21.0F, -154.2F, -8.0F, 6.0F, 6.0F, 4.0F, 0.0F, false);
        foot1.setTextureOffset(96, 73).addBox(21.0F, -157.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(45, 71).addBox(21.0F, -147.4F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(106, 87).addBox(21.0F, -118.2F, -8.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(105, 92).addBox(21.0F, -121.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot1.setTextureOffset(108, 100).addBox(21.0F, -111.4F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

        foot2 = new ModelRenderer(this);
        foot2.setRotationPoint(0.0F, 0.0F, 0.0F);
        posts.addChild(foot2);
        foot2.setTextureOffset(16, 159).addBox(20.0F, -5.0F, 17.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        foot2.setTextureOffset(16, 159).addBox(21.0F, -2.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(16, 159).addBox(21.0F, -7.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(100, 37).addBox(22.0F, -158.0F, 19.0F, 4.0F, 134.0F, 4.0F, 0.0F, false);
        foot2.setTextureOffset(16, 159).addBox(21.0F, -31.0F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(16, 159).addBox(21.0F, -33.6F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(16, 159).addBox(21.0F, -36.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(101, 89).addBox(21.0F, -154.2F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(170, 37).addBox(21.0F, -157.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(116, 72).addBox(21.0F, -147.4F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(109, 79).addBox(21.0F, -118.2F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(116, 87).addBox(21.0F, -121.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot2.setTextureOffset(95, 79).addBox(21.0F, -111.4F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

        foot3 = new ModelRenderer(this);
        foot3.setRotationPoint(0.0F, 0.0F, 0.0F);
        posts.addChild(foot3);
        foot3.setTextureOffset(16, 159).addBox(-28.0F, -5.0F, 17.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        foot3.setTextureOffset(16, 159).addBox(-27.0F, -2.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(16, 159).addBox(-27.0F, -7.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(29, 39).addBox(-26.0F, -158.0F, 19.0F, 4.0F, 134.0F, 4.0F, 0.0F, false);
        foot3.setTextureOffset(16, 159).addBox(-27.0F, -31.0F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(16, 159).addBox(-27.0F, -33.6F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(16, 159).addBox(-27.0F, -36.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(110, 74).addBox(-27.0F, -154.2F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(113, 85).addBox(-27.0F, -157.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(112, 75).addBox(-27.0F, -147.4F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(101, 87).addBox(-27.0F, -118.2F, 18.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(102, 83).addBox(-27.0F, -121.0F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot3.setTextureOffset(108, 99).addBox(-27.0F, -111.4F, 18.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

        foot4 = new ModelRenderer(this);
        foot4.setRotationPoint(0.0F, 0.0F, 0.0F);
        posts.addChild(foot4);
        foot4.setTextureOffset(16, 159).addBox(-28.0F, -5.0F, -9.0F, 8.0F, 4.0F, 8.0F, 0.0F, false);
        foot4.setTextureOffset(16, 159).addBox(-27.0F, -2.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(16, 159).addBox(-27.0F, -7.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(130, 37).addBox(-26.0F, -158.0F, -7.0F, 4.0F, 134.0F, 4.0F, 0.0F, false);
        foot4.setTextureOffset(16, 159).addBox(-27.0F, -31.0F, -8.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(16, 159).addBox(-27.0F, -33.6F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(16, 159).addBox(-27.0F, -36.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(59, 30).addBox(-27.0F, -154.2F, -8.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(93, 64).addBox(-27.0F, -157.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(47, 74).addBox(-27.0F, -147.4F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(126, 97).addBox(-27.0F, -118.2F, -8.0F, 6.0F, 6.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(116, 98).addBox(-27.0F, -121.0F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);
        foot4.setTextureOffset(101, 95).addBox(-27.0F, -111.4F, -8.0F, 6.0F, 2.0F, 6.0F, 0.0F, false);

        torso = new ModelRenderer(this);
        torso.setRotationPoint(0.0F, 0.0F, 0.0F);
        grandfather_clock.addChild(torso);
        torso.setTextureOffset(90, 15).addBox(-20.0F, -165.0F, 20.0F, 40.0F, 144.0F, 2.0F, 0.0F, false);
        torso.setTextureOffset(96, 14).addBox(-24.0F, -157.0F, -4.0F, 4.0F, 136.0F, 26.0F, 0.0F, false);
        torso.setTextureOffset(80, 10).addBox(20.0F, -157.0F, -4.0F, 4.0F, 136.0F, 26.0F, 0.0F, false);
        torso.setTextureOffset(355, 83).addBox(-20.0F, -25.0F, 8.0F, 40.0F, 4.0F, 2.0F, 0.0F, false);

        clockface = new ModelRenderer(this);
        clockface.setRotationPoint(0.0F, 0.0F, -12.0F);
        torso.addChild(clockface);
        

        numbers = new ModelRenderer(this);
        numbers.setRotationPoint(0.0F, 0.0F, 0.5F);
        clockface.addChild(numbers);
        

        twelveToSix = new ModelRenderer(this);
        twelveToSix.setRotationPoint(0.0F, 0.0F, 0.0F);
        numbers.addChild(twelveToSix);
        twelveToSix.setTextureOffset(195, 206).addBox(-1.0F, -152.0F, 11.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        twelveToSix.setTextureOffset(195, 206).addBox(-1.0F, -128.0F, 11.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        fiveTo11 = new ModelRenderer(this);
        fiveTo11.setRotationPoint(0.0F, -138.0F, 13.0F);
        numbers.addChild(fiveTo11);
        setRotationAngle(fiveTo11, 0.0F, 0.0F, 2.618F);
        fiveTo11.setTextureOffset(195, 206).addBox(-1.0F, -14.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        fiveTo11.setTextureOffset(195, 206).addBox(-1.0F, 10.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        fourTo10 = new ModelRenderer(this);
        fourTo10.setRotationPoint(0.0F, -138.0F, 13.0F);
        numbers.addChild(fourTo10);
        setRotationAngle(fourTo10, 0.0F, 0.0F, 2.0944F);
        fourTo10.setTextureOffset(195, 206).addBox(-1.0F, -14.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        fourTo10.setTextureOffset(195, 206).addBox(-1.0F, 10.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        threeToNine = new ModelRenderer(this);
        threeToNine.setRotationPoint(0.0F, -138.0F, 13.0F);
        numbers.addChild(threeToNine);
        setRotationAngle(threeToNine, 0.0F, 0.0F, 1.5708F);
        threeToNine.setTextureOffset(195, 206).addBox(-1.0F, -14.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        threeToNine.setTextureOffset(195, 206).addBox(-1.0F, 10.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        twoToEight = new ModelRenderer(this);
        twoToEight.setRotationPoint(0.0F, -138.0F, 13.0F);
        numbers.addChild(twoToEight);
        setRotationAngle(twoToEight, 0.0F, 0.0F, 1.0472F);
        twoToEight.setTextureOffset(195, 206).addBox(-1.0F, -14.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        twoToEight.setTextureOffset(195, 206).addBox(-1.0F, 10.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        oneToSeven = new ModelRenderer(this);
        oneToSeven.setRotationPoint(0.0F, -138.0F, 13.0F);
        numbers.addChild(oneToSeven);
        setRotationAngle(oneToSeven, 0.0F, 0.0F, 0.5236F);
        oneToSeven.setTextureOffset(195, 206).addBox(-1.0F, -14.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
        oneToSeven.setTextureOffset(195, 206).addBox(-1.0F, 10.0F, -2.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);

        hand_rotate_z = new ModelRenderer(this);
        hand_rotate_z.setRotationPoint(0.0F, -138.0F, 12.0F);
        clockface.addChild(hand_rotate_z);
        hand_rotate_z.setTextureOffset(24, 188).addBox(-1.0F, -12.0F, -1.5F, 2.0F, 12.0F, 1.0F, 0.0F, false);
        hand_rotate_z.setTextureOffset(25, 200).addBox(-1.0F, -2.0F, -0.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);

        side_details = new ModelRenderer(this);
        side_details.setRotationPoint(0.0F, 0.0F, 0.0F);
        torso.addChild(side_details);
        side_details.setTextureOffset(92, 77).addBox(21.0F, -109.0F, 1.0F, 4.0F, 76.0F, 14.0F, 0.0F, false);
        side_details.setTextureOffset(102, 12).addBox(21.0F, -146.0F, 1.0F, 4.0F, 24.0F, 14.0F, 0.0F, false);
        side_details.setTextureOffset(88, 71).addBox(21.0F, -153.0F, -3.0F, 4.0F, 4.0F, 22.0F, 0.0F, false);
        side_details.setTextureOffset(97, 75).addBox(21.0F, -117.0F, -3.0F, 4.0F, 4.0F, 22.0F, 0.0F, false);
        side_details.setTextureOffset(94, 145).addBox(21.0F, -29.0F, -3.0F, 4.0F, 4.0F, 22.0F, 0.0F, false);

        side_details2 = new ModelRenderer(this);
        side_details2.setRotationPoint(0.0F, 0.0F, 0.0F);
        torso.addChild(side_details2);
        side_details2.setTextureOffset(131, 80).addBox(-25.0F, -109.0F, 1.0F, 4.0F, 76.0F, 14.0F, 0.0F, false);
        side_details2.setTextureOffset(123, 66).addBox(-25.0F, -146.0F, 1.0F, 4.0F, 24.0F, 14.0F, 0.0F, false);
        side_details2.setTextureOffset(109, 76).addBox(-25.0F, -153.0F, -3.0F, 4.0F, 4.0F, 22.0F, 0.0F, false);
        side_details2.setTextureOffset(105, 81).addBox(-26.0F, -117.0F, -3.0F, 5.0F, 4.0F, 22.0F, 0.0F, false);
        side_details2.setTextureOffset(100, 134).addBox(-25.0F, -29.0F, -3.0F, 4.0F, 4.0F, 22.0F, 0.0F, false);

        back_details = new ModelRenderer(this);
        back_details.setRotationPoint(0.0F, 0.0F, 0.0F);
        torso.addChild(back_details);
        back_details.setTextureOffset(87, 79).addBox(-18.0F, -109.0F, 15.0F, 36.0F, 76.0F, 8.0F, 0.0F, false);
        back_details.setTextureOffset(60, 61).addBox(-18.0F, -146.0F, 19.0F, 36.0F, 24.0F, 4.0F, 0.0F, false);
        back_details.setTextureOffset(68, 65).addBox(-21.0F, -153.0F, 19.0F, 44.0F, 4.0F, 4.0F, 0.0F, false);
        back_details.setTextureOffset(67, 94).addBox(-25.0F, -117.0F, 19.0F, 48.0F, 4.0F, 4.0F, 0.0F, false);
        back_details.setTextureOffset(71, 161).addBox(-21.0F, -29.0F, 19.0F, 44.0F, 4.0F, 4.0F, 0.0F, false);

        base = new ModelRenderer(this);
        base.setRotationPoint(0.0F, 0.0F, 0.0F);
        grandfather_clock.addChild(base);
        base.setTextureOffset(31, 129).addBox(-32.0F, -20.0F, -9.0F, 64.0F, 8.0F, 34.0F, 0.0F, false);
        base.setTextureOffset(31, 129).addBox(-32.0F, -9.0F, -9.0F, 64.0F, 2.0F, 34.0F, 0.0F, false);
        base.setTextureOffset(31, 129).addBox(-31.0F, -24.0F, -7.0F, 62.0F, 16.0F, 30.0F, 0.0F, false);
        base.setTextureOffset(31, 129).addBox(-32.0F, -25.0F, -9.0F, 64.0F, 2.0F, 34.0F, 0.0F, false);
        base.setTextureOffset(31, 129).addBox(-23.0F, -120.0F, -1.0F, 46.0F, 2.0F, 18.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        glow_clockface.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        
        if (exterior.getOpen() != EnumDoorState.CLOSED)
            door_rotate_y.rotateAngleY = -(float) Math.toRadians(exterior.getOpen() == EnumDoorState.ONE ? 45 : 80);
        else door_rotate_y.rotateAngleY = 0;

        if (exterior.getMatterState() != EnumMatterState.SOLID && exterior.getWorld() != null) {
            this.hand_rotate_z.rotateAngleZ = (exterior.getWorld().getGameTime() * 0.5F) % 360.0F;
            this.pendulum_rotate_z.rotateAngleZ = (float) Math.toRadians(Math.cos(Math.toRadians(exterior.getWorld().getGameTime() * 20.0F % 360.0)) * 6F);
        } else {
            this.pendulum_rotate_z.rotateAngleZ = (float) Math.toRadians(Math.cos(Math.toRadians(exterior.getWorld().getGameTime() * 13.0F % 360.0)) * 6F);
            this.hand_rotate_z.rotateAngleZ = (float) Math.toRadians(exterior.getWorld().getGameTime() * 0.5F % 360.0F);
        }

        door_rotate_y.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        grandfather_clock.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);
        //this.boti.render(matrixStack, buffer, packedLight, packedOverlay);
        glow_clockface.setBright(exterior.getLightLevel());
        glow_clockface.render(matrixStack, buffer, packedLight, packedOverlay, 1, 1, 1, alpha);

    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED) {
            PortalInfo info = new PortalInfo();
            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());
            
            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
            });
            
            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, -0.75, -0.5);
            });
            
            info.setRenderPortal((matrix, buf) -> {
                matrix.push();
                matrix.translate(0, 1.3, 0);
                matrix.scale(0.25F, 0.25F, 0.25F);
                this.boti.render(matrix, buf.getBuffer(RenderType.getEntityTranslucent(ClockExteriorRenderer.TEXTURE)), packedLight, packedOverlay);
                matrix.pop();
            });
            
            BOTIRenderer.addPortal(info);
        }
    }

    @Override
    public void renderBrokenExterior(MatrixStack matrix, IVertexBuilder buffer, int combinedLight, int combinedOverlay) {
    	matrix.push();
    	float scale = 0.25F;
    	matrix.scale(scale, scale, scale);
    	ClientWorld world = ClientHelper.getClientWorldCasted();
        float flickeringLight = 0;
        if(world.rand.nextBoolean()){
            flickeringLight = world.rand.nextFloat();
        }
        this.hand_rotate_z.rotateAngleZ = (world.getGameTime() * 0.5F) % 360.0F;
    	glow_clockface.setBright(flickeringLight);
        glow_clockface.render(matrix, buffer, combinedLight, combinedOverlay);
        door_rotate_y.render(matrix, buffer, combinedLight, combinedOverlay);
        grandfather_clock.render(matrix, buffer, combinedLight, combinedOverlay);
        matrix.pop();
    }
    
}