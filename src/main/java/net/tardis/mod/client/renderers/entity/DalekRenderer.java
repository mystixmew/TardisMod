package net.tardis.mod.client.renderers.entity;

import java.util.HashMap;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.entity.dalek.DalekModel;
import net.tardis.mod.client.models.entity.dalek.DalekSpecialWeaponModel;
import net.tardis.mod.client.models.entity.dalek.IDalekModel;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public class DalekRenderer extends LivingRenderer<DalekEntity, EntityModel<DalekEntity>> {

    private static HashMap<ResourceLocation, EntityModel<DalekEntity>> MODELS = new HashMap<>();
    private static final EntityModel<DalekEntity> dalekModel = new DalekModel();
    private static final EntityModel<DalekEntity> dalekSpecModel = new DalekSpecialWeaponModel();

    public DalekRenderer(EntityRendererManager rendererManager) {
        super(rendererManager, new DalekModel(), 1);
        MODELS.put(TardisConstants.DalekTypes.DALEK_DEFAULT, dalekModel);
        MODELS.put(TardisConstants.DalekTypes.DALEK_RUSTY, dalekModel);
        MODELS.put(TardisConstants.DalekTypes.DALEK_SPEC, dalekSpecModel);
    }

    @Override
    public void render(DalekEntity dalek, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
        entityModel = MODELS.get(dalek.getDalekType().getRegistryName());
        if (entityModel instanceof IDalekModel) {
            IDalekModel dalekModel = (IDalekModel) entityModel;
            dalekModel.setDalek(dalek);
            super.render(dalek, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
        }
    }

    @Override
    protected boolean canRenderName(DalekEntity entity) {
        return entity.getCustomName() != null;
    }

    @Override
    public ResourceLocation getEntityTexture(DalekEntity entity) {
        ResourceLocation texture = entity.getDalekType().getRegistryName();
        return new ResourceLocation(texture.getNamespace(), "textures/entity/dalek/" + entity.getTexture() + ".png");
    }
}
