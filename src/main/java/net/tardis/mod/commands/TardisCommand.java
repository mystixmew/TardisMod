package net.tardis.mod.commands;

import com.mojang.brigadier.CommandDispatcher;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.ArgumentSerializer;
import net.minecraft.command.arguments.ArgumentTypes;
import net.tardis.mod.Tardis;
import net.tardis.mod.commands.argument.ConsoleRoomArgument;
import net.tardis.mod.commands.argument.ConsoleUnitArgument;
import net.tardis.mod.commands.argument.DisguiseArgument;
import net.tardis.mod.commands.argument.ExteriorArgument;
import net.tardis.mod.commands.argument.MissionStageArgument;
import net.tardis.mod.commands.argument.MissionTypeArgument;
import net.tardis.mod.commands.subcommands.ArtronCommand;
import net.tardis.mod.commands.subcommands.AttuneItemCommand;
import net.tardis.mod.commands.subcommands.CreateCommand;
import net.tardis.mod.commands.subcommands.DisguiseCommand;
import net.tardis.mod.commands.subcommands.InteriorCommand;
import net.tardis.mod.commands.subcommands.LoyaltyCommand;
import net.tardis.mod.commands.subcommands.MissionCommand;
import net.tardis.mod.commands.subcommands.RefuelCommand;
import net.tardis.mod.commands.subcommands.RiftCommand;
import net.tardis.mod.commands.subcommands.SetupCommand;
import net.tardis.mod.commands.subcommands.TardisNameCommand;
import net.tardis.mod.commands.subcommands.TraitsCommand;
import net.tardis.mod.commands.subcommands.UnlockCommand;
import net.tardis.mod.helper.Helper;



public class TardisCommand {
	
    public static void register(CommandDispatcher<CommandSource> dispatcher){
        dispatcher.register(
                Commands.literal(Tardis.MODID)
                        .then(SetupCommand.register(dispatcher))
                        .then(UnlockCommand.register(dispatcher))
                        .then(TardisNameCommand.register(dispatcher))
                        .then(TraitsCommand.register(dispatcher))
                        .then(LoyaltyCommand.register(dispatcher))
                        .then(AttuneItemCommand.register(dispatcher))
                        .then(RefuelCommand.register(dispatcher))
                        .then(ArtronCommand.register(dispatcher))
                        .then(InteriorCommand.register(dispatcher))
                        //.then(MissionCommand.register(dispatcher))
                        .then(DisguiseCommand.register(dispatcher))
                        .then(RiftCommand.register(dispatcher))
                        .then(CreateCommand.register(dispatcher))
        );
    }
    
    /** Register our custom Argument Types to allow for proper serialisation on the server
     * <br> Do this in FMLCommonSetup, enqueueWork lambda*/
    public static void registerCustomArgumentTypes() {
    	ArgumentTypes.register(Helper.createRLString("exterior_argument"), ExteriorArgument.class, new ArgumentSerializer<>(ExteriorArgument::getExteriorArgument));
    	ArgumentTypes.register(Helper.createRLString("console_room_argument"), ConsoleRoomArgument.class, new ArgumentSerializer<>(ConsoleRoomArgument::getConsoleRoomArgument));
    	ArgumentTypes.register(Helper.createRLString("console_unit_argument"), ConsoleUnitArgument.class, new ArgumentSerializer<>(ConsoleUnitArgument::getConsoleUnitArgument));
    	ArgumentTypes.register(Helper.createRLString("mission_type_argument"), MissionTypeArgument.class, new ArgumentSerializer<>(MissionTypeArgument::getMissionTypeArgument));
    	ArgumentTypes.register(Helper.createRLString("mission_stage_argument"), MissionStageArgument.class, new ArgumentSerializer<>(MissionStageArgument::getMissionStageArgument));
    	ArgumentTypes.register(Helper.createRLString("disguise_argument"), DisguiseArgument.class, new ArgumentSerializer<>(DisguiseArgument::getDisguiseArgument));
    }
    
    
}
