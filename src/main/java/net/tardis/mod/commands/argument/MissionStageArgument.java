package net.tardis.mod.commands.argument;

import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.missions.stages.MissionStage;
import net.tardis.mod.missions.stages.MissionStages;

public class MissionStageArgument implements ArgumentType<ResourceLocation>{
	private static final Collection<String> EXAMPLES = Stream.of(MissionStages.NOT_STARTED.get()).map((mission) -> {
	      return mission != null ? mission.getRegistryName().toString() : ""; //This could be null when the world is being loaded, as the resource manager is reloaded
	   }).collect(Collectors.toList());
    private static final DynamicCommandExceptionType INVALID_MISSION_STAGE_EXCEPTION = new DynamicCommandExceptionType((mission) -> {
      return new TranslationTextComponent("argument.tardis.mission_stage.invalid", mission);
    });
	
	@Override
	public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
		return ResourceLocation.read(reader);
	}
	
	@Override
	public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
		Collection<ResourceLocation> missions = MissionStages.MISSION_STAGES_REGISTRY.get().getKeys();
		return context.getSource() instanceof ISuggestionProvider ? ISuggestionProvider.func_212476_a(missions.stream(), builder) : Suggestions.empty();
	}

	@Override
	public Collection<String> getExamples() {
		return EXAMPLES;
	}

	public static MissionStageArgument getMissionStageArgument() {
	      return new MissionStageArgument();
	}
	
	public static MissionStage getMissionStage(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
		ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
		MissionStage mission = MissionStages.MISSION_STAGES_REGISTRY.get().getValue(resourcelocation);
		if (mission == null)
			throw INVALID_MISSION_STAGE_EXCEPTION.create(resourcelocation);
		else
			return mission;
	}

}
