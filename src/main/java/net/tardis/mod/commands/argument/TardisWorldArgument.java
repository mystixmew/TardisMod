package net.tardis.mod.commands.argument;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.arguments.ArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.world.dimensions.TDimensions;
/** Custom version of DimensionArgument that only filters for Tardis interior dimension types
 * <br> [Work in Progress]
 * */
public class TardisWorldArgument implements ArgumentType<ResourceLocation> {
   private static final Collection<String> EXAMPLES = Stream.of(World.OVERWORLD, World.THE_NETHER).map((worldKey) -> {
      return worldKey.getLocation().toString();
   }).collect(Collectors.toList());
   private static final DynamicCommandExceptionType INVALID_DIMENSION_EXCEPTION = new DynamicCommandExceptionType((worldKey) -> {
      return new TranslationTextComponent("argument.tardis.dimension.invalid", worldKey);
   });
   @Override
   public ResourceLocation parse(StringReader reader) throws CommandSyntaxException {
      return ResourceLocation.read(reader);
   }
   @Override
   public <S> CompletableFuture<Suggestions> listSuggestions(CommandContext<S> context, SuggestionsBuilder builder) {
       List<RegistryKey<World>> tardisWorldKeys = new ArrayList<>();
       if (context.getSource() instanceof CommandSource) {
    	  CommandSource commandSource = (CommandSource)context.getSource();
          for (RegistryKey<World> worldKey : commandSource.func_230390_p_()) {
        	  World world = commandSource.getServer().getDynamicRegistries().getRegistry(Registry.WORLD_KEY).getValueForKey(worldKey);
              RegistryKey<DimensionType> worldTypeKey = commandSource.getServer().getDynamicRegistries().func_230520_a_().getOptionalKey(world.getDimensionType()).orElse(DimensionType.OVERWORLD);
              if (worldTypeKey == TDimensions.DimensionTypes.TARDIS_TYPE) {
            	  tardisWorldKeys.add(worldKey);
              }
          }
      }
      return context.getSource() instanceof CommandSource ? ISuggestionProvider.func_212476_a(tardisWorldKeys.stream().map(RegistryKey::getLocation), builder) : Suggestions.empty();
   }
   @Override
   public Collection<String> getExamples() {
      return EXAMPLES;
   }

   public static TardisWorldArgument getDimensionArgument() {
      return new TardisWorldArgument();
   }

   public static ServerWorld getTardisWorld(CommandContext<CommandSource> context, String name) throws CommandSyntaxException {
      ResourceLocation resourcelocation = context.getArgument(name, ResourceLocation.class);
      RegistryKey<World> registrykey = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, resourcelocation);
      ServerWorld serverworld = context.getSource().getServer().getWorld(registrykey);
      if (serverworld == null) {
         throw INVALID_DIMENSION_EXCEPTION.create(resourcelocation);
      } else {
         return serverworld;
      }
   }
}
