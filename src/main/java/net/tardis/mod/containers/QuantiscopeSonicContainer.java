package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;

public class QuantiscopeSonicContainer extends QuantiscopeContainer{
	
	//Needed for customization
	public BlockPos pos;
	/** Client Only constructor */
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, PacketBuffer extraData) {
		super(TContainers.QUANTISCOPE.get(), id);
		pos = extraData.readBlockPos();
		TileEntity te = inv.player.world.getTileEntity(pos);
		if(te instanceof QuantiscopeTile) {
			init((QuantiscopeTile)te, inv);
		}
	}
	/** Server Only constructor */
	public QuantiscopeSonicContainer(int id, PlayerInventory inv, QuantiscopeTile workbench) {
		super(TContainers.QUANTISCOPE.get(), id);
		init(workbench, inv);
	}
	
	public void init(QuantiscopeTile workbench, PlayerInventory inv) {
		this.blockEntity = workbench;
		
		this.addSlot(new SonicSlot(workbench, 0, 42, 47));
        
		//Player Inventory
	    TInventoryHelper.addPlayerInvContainer(this, inv, 0, -2);
	}

	public void setSlotChangeAction(Runnable run) {
		if(this.getSlot(0) instanceof SonicSlot)
			((SonicSlot)this.getSlot(0)).setOnSlotChanged(run);
	}
	
	public static class SonicSlot extends SlotItemHandler{

		private Runnable onSlotChanged;
		
		public SonicSlot(IItemHandler itemHandler, int index, int xPosition, int yPosition) {
			super(itemHandler, index, xPosition, yPosition);
		}

		@Override
		public boolean isItemValid(ItemStack stack) {
			return stack.getItem() == TItems.SONIC.get();
		}

		@Override
		public void onSlotChanged() {
			super.onSlotChanged();
			if(onSlotChanged != null)
				onSlotChanged.run();
		}
		
		public void setOnSlotChanged(Runnable run) {
			this.onSlotChanged = run;
		}
		
	}
	
}
