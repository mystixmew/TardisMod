package net.tardis.mod.controls;

import javax.annotation.Nullable;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.tileentities.ConsoleTile;
/** Main implementation of {@linkplain AbstractControl}, extend this for your custom controls*/
public abstract class BaseControl extends AbstractControl{

//	private ResourceLocation registryName; //Deprecated, leftover from when AbstractControl (formerly IControl) used to be an instance of Spectre's IRegisterable
	private ConsoleTile console;
	@Nullable
	protected String translationKey;
	@Nullable
	private TranslationTextComponent translation;
	@Nullable
	private ResourceLocation additionalDataSaveKey;
	private int animTicks = 0;
	private boolean isDirty = false;
	private boolean isGlowing = false;
	private ControlEntity parent;
	
	public BaseControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry);
		this.console = console;
		this.parent = entity;
	}

	@Override
	public void setConsole(ConsoleTile console, ControlEntity entity) {
		this.console = console;
		this.parent = entity;
	}

	@Override
	public ConsoleTile getConsole() {
		return this.console;
	}
	
	@Override
	public String getTranslationKey() {
		if (this.translationKey == null) {
			this.translationKey = Util.makeTranslationKey("control", ControlRegistry.CONTROL_REGISTRY.get().getKey(getEntry()));
		}
		return this.translationKey;
	}
	

	@Override
	public TranslationTextComponent getDisplayName() {
		this.translation = new TranslationTextComponent(this.getTranslationKey());
		return this.translation;
	}

	@Override
	public int getAnimationTicks() {
		return this.animTicks;
	}

	@Override
	public void setAnimationTicks(int ticks) {
		this.animTicks = ticks;
		this.markDirty();
	}

	@Override
	public void onPacketUpdate() {}
	
	@Override
	public void markDirty() {
		this.isDirty = true;
	}
	
	@Override
	public void clean() {
		this.isDirty = false;
	}
	
	@Override
	public boolean isDirty() {
		return this.isDirty;
	}

	@Override
	public boolean isGlowing() {
		return this.isGlowing;
	}

	@Override
	public void setGlow(boolean glow) {
		this.isGlowing = glow;
	}
	
	public void startAnimation() {
		this.setAnimationTicks(this.getMaxAnimationTicks());
	}

	@Override
	public int getMaxAnimationTicks() {
		return 20;
	}

	@Override
	public float getAnimationProgress() {
		return this.getAnimationTicks() / (float)this.getMaxAnimationTicks();
	}
	
	@Override
	public ResourceLocation getAdditionalDataSaveKey() {
		return this.additionalDataSaveKey;
	}
	
	public ControlEntity getEntity() {
		return this.parent;
	}
	
}
