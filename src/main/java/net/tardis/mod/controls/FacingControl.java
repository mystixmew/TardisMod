	package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class FacingControl extends BaseControl{

	private Direction facing = Direction.NORTH;
	
	public FacingControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F,  2 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
	        return EntitySize.flexible(0.125F, 0.125F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		return EntitySize.flexible(3 / 16.0F, 5 / 16.0F);
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-9 / 16.0, 8 / 16.0, -9 / 16.0);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.7350152138595012, 0.39374999701976776, 0.20420024945559656);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.24328874178188886, 0.53125, 0.425);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.648, 0.5, 0.225);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.763, 0.406, -0.185);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(1.1245386855892283, 0.125, -0.015023021137454462);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.3273009412682333, 0.4375, 1.064750650987496);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.9364063741581483, 0.40625, 0.2252643279138704);
		
		return new Vector3d(-5.3 / 16.0, 5 / 16.0, 15 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		int index = this.getDirection().getHorizontalIndex() + (player.isSneaking() ? -1 : 1);
		if(index < 0)
			index = 3;
		else if(index > 3)
			index = 0;
		
		facing = Direction.byHorizontalIndex(index);
		if(!player.world.isRemote) {
			console.setExteriorFacingDirection(facing);
			player.sendStatusMessage(new TranslationTextComponent("message.tardis.control.facing").appendSibling(new StringTextComponent(facing.getName2().toUpperCase()).mergeStyle(TextFormatting.LIGHT_PURPLE)), true);
			this.setAnimationTicks(20);
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.DIRECTION.get();
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
	
	public void setDirection(Direction facing) {
		this.facing = facing;
	}
	
	public Direction getDirection() {
		return this.facing;
	}
	

}
