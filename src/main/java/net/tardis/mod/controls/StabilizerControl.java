package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants.Translations;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.IStabilizerSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class StabilizerControl extends BaseControl{
	
	public StabilizerControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize(){
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			console.getSubsystem(EnumSubsystemType.STABILIZER).ifPresent(sys -> {
			    if (sys.canBeUsed(console)) {
					((IStabilizerSubsystem)sys).setControlActivated(!((IStabilizerSubsystem)sys).isControlActivated(console), console);
	                player.sendStatusMessage(new TranslationTextComponent("message.tardis.control.stabilizer." + ((IStabilizerSubsystem)sys).isControlActivated(console)), true);
			    }
			    else {
			        ItemStack name = new ItemStack(TItems.STABILIZERS.get());
                    player.sendStatusMessage(new TranslationTextComponent(Translations.NO_COMPONENT, name.getDisplayName().getString()), true);
			    }
			});
			this.setAnimationTicks(20);
			this.markDirty();
		}
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vector3d(13.5 / 16.0, 7 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.5455551641720471, 0.425, -0.5866870884910467);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.5442238848074868, 0.34375, 0.7);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.166, 0.344, 0.728);

		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(0, 0.375, 0.756);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.45423586733985566, 0.3125, -0.769970078434481);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.018837774181529765, 0.375, -1.0038470729956595);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.6131817947812537, 0.4375, -0.49161027485812314);
		
		return new Vector3d(-7 / 16.0, 10 / 16.0, 4 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		ObjectWrapper<SoundEvent> sound = new ObjectWrapper<SoundEvent>(TSounds.STABILIZER_ON.get());
		console.getSubsystem(EnumSubsystemType.STABILIZER).ifPresent(sys -> {
			if(!((IStabilizerSubsystem)sys).isControlActivated(console))
				sound.setValue(TSounds.STABILIZER_OFF.get());
		});
		return sound.getValue();
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public void setConsole(ConsoleTile console, ControlEntity entity) {
		super.setConsole(console, entity);
	}

}
