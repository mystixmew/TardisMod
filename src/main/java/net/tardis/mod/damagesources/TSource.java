package net.tardis.mod.damagesources;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class TSource extends DamageSource {

    private String message;
    private boolean bypassesArmor;
    
    public TSource(String name) {
        this(name, false);
    }
    
    public TSource(String name, boolean bypassesArmor) {
        super(name);
        this.message = "damagesrc.tardis." + name;
        this.bypassesArmor = bypassesArmor;
    }

    @Override
    public ITextComponent getDeathMessage(LivingEntity entity) {
        return new TranslationTextComponent(message, entity.getDisplayName());
    }

    @Override
    public boolean isUnblockable() {
        return !bypassesArmor;
    }

}