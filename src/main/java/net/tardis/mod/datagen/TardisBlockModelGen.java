package net.tardis.mod.datagen;

import java.nio.file.Path;

import net.minecraft.block.Block;
import net.minecraft.block.SlabBlock;
import net.minecraft.block.WallBlock;
import net.minecraft.data.DataGenerator;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.blocks.TechStrutBlock;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.blocks.exteriors.TardisExteriorBottomBlock;

public class TardisBlockModelGen extends BlockModelProvider {
	public final Lazy<ModelFile> EMPTY_MODEL = Lazy.of(() -> getExistingFile(mcLoc("air")));

	private final DataGenerator generator;
	
    public TardisBlockModelGen(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, Tardis.MODID, existingFileHelper);
		this.generator = generator;
	}
	
	@Override
	protected void registerModels() {
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().equals(Tardis.MODID)) {
			    if (!(block instanceof ConsoleBlock) && !(block instanceof ExteriorBlock) && !(block instanceof TardisExteriorBottomBlock) && !(block instanceof WallBlock) && !(block instanceof SlabBlock) && !(block instanceof TechStrutBlock)) {
			    	//this.cubeAll(block.getRegistryName().getPath(), block.getRegistryName());
			    }
			}
		}
	}
	
	
	public static Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/models/block/" + key.getPath() + ".json");
	}
}
