package net.tardis.mod.entity;

import static net.minecraft.block.CampfireBlock.spawnSmokeParticles;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.world.World;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.sounds.TSounds;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 23:05
 */
public class BessieEntity extends CreatureEntity implements IMob {

    public BessieEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
        super(type, worldIn);
    }

    public BessieEntity(World worldIn) {
        super(TEntities.BESSIE.get(), worldIn);
    }

    private static int MAX_PASSENGERS = 3;

    private long lastHorn = 0;

    public long getLastHorn() {
        return lastHorn;
    }

    public void setLastHorn(long lastHorn) {
        this.lastHorn = lastHorn;
    }
    
    public static AttributeModifierMap.MutableAttribute createAttributes() {
    	return CreatureEntity.func_233666_p_().createMutableAttribute(Attributes.MAX_HEALTH, 20D);
    }

    @Override
    public ActionResultType applyPlayerInteraction(PlayerEntity player, Vector3d vec, Hand hand) {
        if (!player.isSneaking() && getPassengers().size() < MAX_PASSENGERS && player.getHeldItemMainhand().isEmpty()) {
            player.startRiding(this);
        }

        //TODO Add a actual way of repairing
        if (player.isSneaking() && player.getHeldItemMainhand().getItem() == TItems.SONIC.get()) {
            heal(1);
        }
        return super.applyPlayerInteraction(player, vec, hand);
    }

    //TODO: Spec to add seating
    @Override
    public void updatePassenger(Entity pass) {
        if (getPassengers().size() > 0 && getPassengers().get(0) == pass) {
            Vector3d pos = (getLookVec().rotateYaw(-90).scale(0.5)).add(getPositionVec());
            pass.setPosition(pos.x, pos.y, pos.z);
        }
        if (getPassengers().size() > 1 && getPassengers().get(1) == pass) {
            Vector3d pos = (getLookVec().rotateYaw(90).scale(0.5)).add(getPositionVec());
            pass.setPosition(pos.x, pos.y, pos.z);
        }
        if (getPassengers().size() > 2 && getPassengers().get(2) == pass) {
            Vector3d pos = (getLookVec().scale(-1)).add(getPositionVec());
            pass.setPosition(pos.x, pos.y, pos.z);
        }
    }

    @Override
    protected boolean canFitPassenger(Entity passenger) {
        return getPassengers().size() < MAX_PASSENGERS;
    }


    @Override
    public void applyEntityCollision(Entity entityIn) {
        if (!(entityIn instanceof PlayerEntity) && getPassengers().size() < MAX_PASSENGERS && world.getGameTime() % 20 == 0 && entityIn.getRidingEntity() == null) {
            entityIn.startRiding(this);
        }
        super.applyEntityCollision(entityIn);
    }


    @Override
    public double getMountedYOffset() {
        return 0.5D;
    }

    @Override
    public void tick() {
        setCustomNameVisible(hasCustomName());

        boolean canDrive = getHealth() > 5;

        if (getPassengers().isEmpty() && onGround) {
            setMotion(0, 0, 0);
        }

        //You are a car, you do not choke, you do not have lungs
        setAir(getMaxAir());

        //Client
        if (world.isRemote) {
            if (ticksExisted == 10) {

                //*Happy vroom vroom noises*
            	ClientHelper.playMovingSound(this, TSounds.BESSIE_DRIVE.get(), SoundCategory.NEUTRAL, 0.3F, true);
            }

            if (!canDrive) {
                //Your engine is broken, so it emits the steam
                BlockPos pos = getPosition();
                Vector3d look = getLookVec();
                spawnSmokeParticles(world, pos.offset(Direction.getFacingFromVector(look.x, look.y, look.z)), false, false);
            }
        }

        //Common
        super.tick();
   /*
        //This was code for a concept where Bessie would light up the area around her, but blocks take a few seconds long to become air
        float motion = MathHelper.sqrt(Entity.horizontalMag(getMotion()));
        boolean isMoving = motion >= 0.01D;
        boolean isDevFeatureActive = false; //Inpartial about adding this feature

     if (!world.isRemote) {
            if (isMoving && isDevFeatureActive) {
                Vec3d look = getLookVec();
                BlockPos frontLightPos = getPosition().offset(Direction.getFacingFromVector(look.x, look.y, look.z), 2);
                BlockPos backLightPos = getPosition().offset(Direction.getFacingFromVector(-look.x, -look.y, -look.z), 2);
                if (world.isAirBlock(frontLightPos)) {
                    world.setBlockState(frontLightPos, TBlocks.lightBlock.getDefaultState());
                    world.getPendingBlockTicks().scheduleTick(frontLightPos, TBlocks.lightBlock, 0);
                }
                if (world.isAirBlock(backLightPos)) {
                    world.setBlockState(backLightPos, TBlocks.lightBlock.getDefaultState());
                    world.getPendingBlockTicks().scheduleTick(backLightPos, TBlocks.lightBlock, 0);
                }
            }

        }*/
    }

    @Override
    public void travel(Vector3d p_213352_1_) {

        float motion = MathHelper.sqrt(Entity.horizontalMag(getMotion()));
        boolean isMoving = motion >= 0.01D;

//        if (isAlive() && getHealth() > 5 && !isInWater()) {
        if (isAlive() && getHealth() > 5) { //Temp fix to Bessie getting stuck in water and never being able to driven ever again
            if (isBeingRidden() && canBeSteered()) {
                if (getControllingPassenger() != null) {
                    LivingEntity livingentity = (LivingEntity) getControllingPassenger();

                    if (isMoving) {
                        rotationYaw = livingentity.rotationYaw;
                    }

                    prevRotationYaw = rotationYaw;
                    rotationPitch = livingentity.rotationPitch * 0.5F;
                    setRotation(rotationYaw, rotationPitch);
                    renderYawOffset = rotationYaw;
                    rotationYawHead = renderYawOffset;

                    float moveForwardAmount = livingentity.moveForward;
                    if (moveForwardAmount <= 0.0F) {
                        moveForwardAmount *= 0.25F;
                    }

                    stepHeight = 1.0F;

                    if (canPassengerSteer()) {
                        setAIMoveSpeed(0.3F);
                        super.travel(new Vector3d(0, p_213352_1_.y, (double) moveForwardAmount));
                    }
                }
            } else {
                super.travel(p_213352_1_);
            }
        }
    }

    @Override
    public boolean getAlwaysRenderNameTagForRender() {
        return super.getAlwaysRenderNameTagForRender();
    }

    @Override
    public boolean canBeRiddenInWater() {
        return true;
    }

    @Nullable
    @Override
    public Entity getControllingPassenger() {
        if (!getPassengers().isEmpty()) {
            return getPassengers().get(0);
        }
        return null;
    }

    @Override
    public boolean canBeSteered() {
        return true;
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
    }

    @Override
    protected void playSwimSound(float volume) {
    }
    
    @Override
    public boolean isImmuneToFire() {
        return true;
    }
    
    @Override
    public boolean canRenderOnFire() {
        return false;
    }
}
