package net.tardis.mod.entity.hostile;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemTier;
import net.minecraft.item.ToolItem;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.tardis.api.space.entities.ISpaceImmuneEntity;

public class CybermanEntity extends MobEntity implements ISpaceImmuneEntity{

	public CybermanEntity(EntityType<? extends MobEntity> type, World worldIn) {
		super(type, worldIn);
	}

	@Override
	public void tick() {
		super.tick();
	}

	@Override
	public boolean canBreatheUnderwater() {
		return true;
	}


	@Override
	public boolean attackEntityAsMob(Entity attacker) {
		return super.attackEntityAsMob(attacker);
	}

	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if(source.getImmediateSource() instanceof PlayerEntity) {
			PlayerEntity player = (PlayerEntity)source.getImmediateSource();
			if(player.getHeldItemMainhand().getItem() instanceof ToolItem) {
				if(((ToolItem)player.getHeldItemMainhand().getItem()).getTier() == ItemTier.GOLD) {
					amount *= 1.5F;
				}
			}
		}
		return super.attackEntityFrom(source, amount);
	}

	@Override
	public boolean shouldTakeSpaceDamage() {
		return false;
	}

	

}
