package net.tardis.mod.entity.humanoid;

public enum HumanoidEmotionalState {
	HAPPY,
	RELIEVED,
    FRUSTRATED,
    HOSTILE,
}
