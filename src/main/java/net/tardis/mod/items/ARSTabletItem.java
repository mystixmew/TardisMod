package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.constants.TardisConstants.Gui;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.world.dimensions.TDimensions;

public class ARSTabletItem extends TooltipProviderItem {

    public ARSTabletItem() {
        super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    @Nullable
    public static ARSPiece getSelectedPiece(ItemStack stack) {
        CompoundNBT tag = stack.getOrCreateTag();
        if (tag.contains("piece"))
            return ARSPiece.getRegistry().get(new ResourceLocation(tag.getString("piece")));
        return null;
    }

    public static void setPiece(ItemStack stack, ARSPiece piece) {
        stack.getOrCreateTag().putString("piece", piece.getRegistryName().toString());
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        PlayerEntity player = context.getPlayer();
        if (context.getHand() != null && !context.getWorld().isRemote()) {
        	boolean corridorBlock = context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn.get();
            if (corridorBlock) {
                //If in TARDIS
                if (WorldHelper.areDimensionTypesSame(context.getWorld(), TDimensions.DimensionTypes.TARDIS_TYPE)) {
                    //If console is in world
                    TardisHelper.getConsoleInWorld(context.getWorld()).ifPresent(tile -> {
                        //If is an admin
                        if (tile.canDoAdminFunction(player)) {
                            //Spawn the structure, must be scheduled
                            context.getWorld().getServer().enqueue(new TickDelayedTask(1, () -> {
                                ARSPiece piece = getSelectedPiece(context.getItem());
                                player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                                    if (cap.getCountdown() > 0) {//Stop players from creating new rooms while trying to delete a room
                                        player.sendStatusMessage(new TranslationTextComponent("message.tardis.ars.room.delete.concurrent_mod.denied"), true);
                                    } else {
                                        if (piece != null) {
                                            piece.spawn((ServerWorld) context.getWorld(), context.getPos(), player, context.getFace().getOpposite());
                                            context.getWorld().playSound(null, context.getPos(), SoundEvents.BLOCK_BEACON_ACTIVATE, SoundCategory.BLOCKS, 1F, 1F);
                                        } else {
                                            player.sendStatusMessage(new TranslationTextComponent("message.tardis.ars.structure_null"), true);
                                        }
                                    }
                                });
                            }));
                        } else player.sendStatusMessage(TardisConstants.Translations.NOT_ADMIN, true);
                    });
                }
            }


        }

        //If kill
        if (context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_kill.get() && context.getWorld().isRemote) {
            ClientHelper.openGUI(Gui.ARS_TABLET_KILL, new BlockPosGuiContext(context.getPos()));
            return ActionResultType.SUCCESS;
        }
        return context.getWorld().getBlockState(context.getPos()).getBlock() == TBlocks.corridor_spawn.get() ? ActionResultType.SUCCESS : super.onItemUse(context);
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        if (worldIn.isRemote)
            ClientHelper.openGUI(TardisConstants.Gui.ARS_TABLET, null);
        return super.onItemRightClick(worldIn, playerIn, handIn);
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.spawn"));
        tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.remove"));
    }

	@Override
	public void createDefaultTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip.ars_tablet.piece").appendSibling(getSelectedPiece(stack) == null ? new StringTextComponent("None").mergeStyle(TextFormatting.LIGHT_PURPLE) : 
        	getSelectedPiece(stack).isDataPack() ? new StringTextComponent(getSelectedPiece(stack).getDisplayName().getKey()) : getSelectedPiece(stack).getDisplayName().mergeStyle(TextFormatting.LIGHT_PURPLE)));
	}
    
    


}
