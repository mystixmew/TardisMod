package net.tardis.mod.items;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.*;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.misc.TooltipProviderItem;
import net.tardis.mod.misc.Console;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.console.DataTypes;
import net.tardis.mod.network.packets.console.SubsystemData;
import net.tardis.mod.registries.TardisStatistics;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;
import net.tardis.mod.tileentities.inventory.PanelInventory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class SubsystemItem extends TooltipProviderItem implements ISubsystemType{

    protected EnumSubsystemType type;
    protected boolean requiredForFlight;
    protected boolean requiresRepair;
    protected boolean canBeUsed = false;
    public boolean isActive;

    public SubsystemItem(Item.Properties prop, EnumSubsystemType type, boolean required, boolean repairable){
        super(prop);
        this.type = type;
        this.requiredForFlight = required;
        this.requiresRepair = repairable;
        this.setShowTooltips(true);
        this.setHasStatisticsTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    @Override
    public EnumSubsystemType getType() {
        return type;
    }

    public void setCanBeUsed(boolean canBeUsed){
        this.canBeUsed = canBeUsed;
    }

    public boolean canBeUsed(ConsoleTile console){
        if(console.getWorld().isRemote)
            return this.canBeUsed;

        if(!this.isActive())
            return false;

        return (this.canBeUsed = this.getItem().getDamage(this.getDefaultInstance()) < this.getItem().getMaxDamage());
    }

    public boolean stopsFlight(ConsoleTile console) {
        return !this.canBeUsed(console);
    }

    public void onTakeoff(ConsoleTile console){}

    public void onLand(ConsoleTile console){};

    public void onFlightSecond(ConsoleTile console){};

    public void onBreak(ConsoleTile console){};

    public ItemStack getItem(ConsoleTile console) {
        if(console != null && console.hasWorld()) {
            ITardisWorldData data = console.getWorld().getCapability(Capabilities.TARDIS_DATA).orElse(null);
            if(data != null) {
                PanelInventory inv = data.getEngineInventoryForSide(Direction.NORTH);
                for(int i = 0; i < inv.getSlots(); ++i) {
                    if(inv.getStackInSlot(i).getItem() == this.asItem())
                        return inv.getStackInSlot(i);
                }
            }
        }
        return ItemStack.EMPTY;
    }

    public boolean isActive(){
        return this.isActive;
    }
    public void setActive(boolean active){
        this.isActive = active;
    }

    public void damage(@Nullable ServerPlayerEntity player, int amt, ConsoleTile console) {
        this.getItem(console).attemptDamageItem(amt, console.getWorld().getRandom(), player);
        //Removes it if less than 0 health
        if(this.getItem(console).getDamage() >= this.getItem(console).getMaxDamage()) {
            console.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
                PanelInventory inv = cap.getEngineInventoryForSide(Direction.NORTH);
                for (int i = 0; i < inv.getSlots(); ++i) {
                    if (inv.getStackInSlot(i).getItem() == this.getItem(console).getItem()) {
                        inv.setStackInSlot(i, ItemStack.EMPTY);
                        Helper.addTardisStatistic(player, TardisStatistics.SUBSYSTEMS_BROKEN);
                        break;
                    }
                }
            });
        }


        if(!this.canBeUsed(console))
            this.onBreak(console);

        this.updateClientIfNeeded(console);
    }

    /**
     *
     * @return If this subsystem should make the console spark, non-essential probably should just return false
     */
    public SparkingLevel getSparkState() {
        ItemStack stack = this.getDefaultInstance();

        // Non-damagable items and stop / by 0
        if (stack.getMaxDamage() <= 0)
            return SparkingLevel.NONE;

        int maxDamage = stack.getMaxDamage();
        int currentDamage = stack.getDamage();

        if (currentDamage > maxDamage / 2)
            return SparkingLevel.SPARKS;

        if (currentDamage > maxDamage - 25)
            return SparkingLevel.SMOKE;

        return SparkingLevel.NONE;
    }

    /**
     *
     * @param softCrash - if this is true, was just a missed control, otherwise was a subsystem failure or something to knock it out of flight
     */
    public void explode(ConsoleTile console, boolean softCrash) {
        this.damage(null, softCrash ? 10 : 50, console);
    }

    private void updateClientIfNeeded(ConsoleTile console) {

        if(console.getWorld().isRemote)
            return;

        if(this.canBeUsed != this.canBeUsed(console))
            Network.sendToAllInWorld(new ConsoleUpdateMessage(DataTypes.SUBSYSTEM, new SubsystemData(this.type, this.canBeUsed(console), this.isActive())), (ServerWorld)console.getWorld());
    }

    @Override
    public void createStatisticTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
                                        ITooltipFlag flagIn) {
        String typeName = this.type.toString().toLowerCase();
        String typeFriendlyName = typeName.substring(0, 1).toUpperCase() + typeName.substring(1);
        StringTextComponent panelName = new StringTextComponent("Components");
        tooltip.add(new TranslationTextComponent("tooltip.part.type").appendSibling(new StringTextComponent(typeFriendlyName).mergeStyle(TextFormatting.LIGHT_PURPLE)));

        tooltip.add(new TranslationTextComponent("tooltip.part.flight.required").appendSibling(new StringTextComponent(String.valueOf(this.requiredForFlight)).mergeStyle(TextFormatting.LIGHT_PURPLE)));

        tooltip.add(new TranslationTextComponent("tooltip.part.repair.required").appendSibling(new StringTextComponent(String.valueOf(this.requiresRepair)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        tooltip.add(new TranslationTextComponent("tooltip.part.engine_panel").appendSibling(panelName.mergeStyle(TextFormatting.BLUE)));
    }

    @Override
    public void createDescriptionTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
            tooltip.add(TardisConstants.Prefix.TOOLTIP_ITEM_DESCRIPTION.deepCopy().appendSibling(new TranslationTextComponent("tooltip.part." + this.getRegistryName().getPath() + ".description").mergeStyle(TextFormatting.GRAY)));
    }


}
