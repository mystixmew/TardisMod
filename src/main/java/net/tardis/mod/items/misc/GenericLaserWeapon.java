package net.tardis.mod.items.misc;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import net.minecraft.util.DamageSource;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.AbstractWeapon;
import net.tardis.mod.sounds.TSounds;

public class GenericLaserWeapon<T extends LivingEntity> extends AbstractWeapon<T>{
    protected T user;
    protected DamageSource source;
    protected Vector3d laserColour;
    protected float rayLength;
    protected SoundEvent sound;

    public GenericLaserWeapon(T user, DamageSource source, float damage, Vector3d colour, float rayLength, SoundEvent sound) {
        this.user = user;
        this.source = source;
        this.laserColour = colour;
        this.damageAmount = damage;
        this.rayLength = rayLength;
        this.sound = sound;
    }
    
    public GenericLaserWeapon(T user) {
        this(user, TDamageSources.causeTardisMobDamage(TDamageSources.LASER, user), 5F, new Vector3d(0, 1, 1), 1.0F, TSounds.LASER_GUN_FIRE.get());
    }

    @Override
    public void onHitEntityPre(LaserEntity laserEntity, Entity hitEntity) {
        if (!this.user.world.isRemote()) {
        	if (hitEntity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity)hitEntity;
                if (player.getHeldItemOffhand().getItem() instanceof ShieldItem || player.getHeldItemMainhand().getItem() instanceof ShieldItem) {
                    ItemStack heldStack = PlayerHelper.isInMainHand(user, TItems.EARTHSHOCK_GUN.get()) ? user.getHeldItemMainhand() : user.getHeldItemOffhand();
                    WorldHelper.maybeDisableShieldNoEnchant(player, user, heldStack, player.getHeldItemOffhand());
                }
            }
        }
    }

	@Override
    public boolean useWeapon(LivingEntity entity) {
        if (!entity.world.isRemote) {
            LaserEntity laser = new LaserEntity(TEntities.LASER.get(), entity, entity.world, damage(), this.source);
            //Setup laser fire direction, velocity and inaccuracy
            laser.setDirectionAndMovement(laser, entity.rotationPitch, entity.rotationYawHead, 0.0F, 1.5F, 0.1F);
            laser.setRayLength(this.rayLength);
            laser.setColor(this.laserColour);
            laser.setWeaponType(this);
            entity.world.addEntity(laser);
            entity.world.playSound(null, entity.getPosition(), this.sound, SoundCategory.HOSTILE, 1F, 1F);
            return true;
        }
        return false;
    }
    
    
}
