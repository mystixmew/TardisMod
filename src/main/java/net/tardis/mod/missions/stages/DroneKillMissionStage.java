package net.tardis.mod.missions.stages;

import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;

public class DroneKillMissionStage extends MissionStage{
	
    public static final TranslationTextComponent KILL_DRONES_DESCRIPTION = new TranslationTextComponent("missions.tardis.objective.drone_station.kill_drone");
    private static final IFormattableTextComponent OBJECTIVE_KILL_DRONES = TardisConstants.Translations.MISSION_OBJECTIVE.deepCopy().appendString(" - ").appendSibling(KILL_DRONES_DESCRIPTION);

	public DroneKillMissionStage() {
		super(15);
	}

	@Override
	public IFormattableTextComponent getDisplayNameForObjective(int objective) {
		return OBJECTIVE_KILL_DRONES;
	}
}
