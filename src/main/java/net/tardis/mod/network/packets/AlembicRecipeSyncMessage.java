package net.tardis.mod.network.packets;


import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.recipe.AlembicRecipe;
/** Previous packet used to sync {@linkplain AlembicRecipe}(s) to the client
 * <br> No longer used as we are using vanilla's {@linkplain IRecipe} which handles all syncing for us*/
@Deprecated
public class AlembicRecipeSyncMessage {

	private Map<ResourceLocation, AlembicRecipe> recipes = new HashMap<ResourceLocation, AlembicRecipe>();
	
	public AlembicRecipeSyncMessage(Map<ResourceLocation, AlembicRecipe> recipes) {
        this.recipes = recipes;
	}
	
	public static void encode(AlembicRecipeSyncMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.recipes.size());
		for (AlembicRecipe recipe : mes.recipes.values()) {
			buf.writeResourceLocation(recipe.getId());
        	recipe.encode(buf);
		}
	}
	
	public static AlembicRecipeSyncMessage decode(PacketBuffer buf) {
		
		//Read alembic
		Map<ResourceLocation, AlembicRecipe> recipes = new HashMap<>();
		int alembicSize = buf.readInt();
		for(int i = 0; i < alembicSize; ++i) {
			recipes.put(buf.readResourceLocation(), new AlembicRecipe(buf));
		}
		
		return new AlembicRecipeSyncMessage(recipes);

	}
	
	public static void handle(AlembicRecipeSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			AlembicRecipe.DATA_LOADER.setData(mes.recipes);
		});
		cont.get().setPacketHandled(true);
	}
}
