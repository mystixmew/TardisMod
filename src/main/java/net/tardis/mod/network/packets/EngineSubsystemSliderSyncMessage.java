package net.tardis.mod.network.packets;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.registries.UpgradeRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

public class EngineSubsystemSliderSyncMessage {
    public Map<EnumSubsystemType, Boolean> subsystemStates =  new HashMap<>();

    public EngineSubsystemSliderSyncMessage(Map<EnumSubsystemType, Boolean> subsystemStates) {
        this.subsystemStates = subsystemStates;
    }
    
    public static void encode(EngineSubsystemSliderSyncMessage mes, PacketBuffer buf) {
        for(Entry<EnumSubsystemType, Boolean> e : mes.subsystemStates.entrySet()) {
            buf.writeEnumValue(e.getKey());
            buf.writeBoolean(e.getValue());
        }
        
    }
    
    public static EngineSubsystemSliderSyncMessage decode(PacketBuffer buf){
        Map<EnumSubsystemType, Boolean> subsystemStates = new HashMap<>();
        EnumSubsystemType[] value = EnumSubsystemType.values();
        for(int i = 0; i < value.length-2; ++i){
            subsystemStates.put(buf.readEnumValue(EnumSubsystemType.class), buf.readBoolean());
        }
        return new EngineSubsystemSliderSyncMessage(subsystemStates);
    }
    
    public static EngineSubsystemSliderSyncMessage create(ServerWorld world) {
        Map<EnumSubsystemType, Boolean> map = getSubsystemStates(world);
        return new EngineSubsystemSliderSyncMessage(map);
    }
    
    public static void handle(EngineSubsystemSliderSyncMessage mes, Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> {
            if (context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER) {
                Map<EnumSubsystemType, Boolean> map = getSubsystemStates(context.get().getSender().getEntityWorld());
                Network.sendTo(new EngineSubsystemSliderSyncMessage(map), context.get().getSender());
            }
            else {
                ClientPacketHandler.handleEngineSubsystemSliderSyncClient(mes);
            }
            context.get().setPacketHandled(true);
        });
    }
    
    public static Map<Integer, Boolean> getUpgradeStates(int maxSize, World world){
        Map<Integer, Boolean> upgradeStates = new HashMap<>();
        world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            for (int i = 0; i < maxSize; i++) {
                ItemStack stack = cap.getEngineInventoryForSide(Direction.SOUTH).getStackInSlot(i);
                if (stack != null) {
                    if (!stack.isEmpty()) {
                        UpgradeEntry entry = UpgradeRegistry.getUpgradeFromItem(stack.getItem());
                        if (entry != null) {
                            if (TardisHelper.getConsoleInWorld(world).isPresent()) {
                                ConsoleTile tile = TardisHelper.getConsoleInWorld(world).get();
                                if (tile != null) {
                                    if(tile.getUpgrade(entry.getRegistryName()).isPresent()) {
                                        Upgrade upgrade = tile.getUpgrade(entry.getRegistryName()).get();
                                        upgradeStates.put(i, upgrade.isActivated());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        return upgradeStates;
    }

    public static Map<EnumSubsystemType, Boolean> getSubsystemStates(World world){
        Map<EnumSubsystemType, Boolean> subsystemStates = new HashMap<>();
        world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            EnumSubsystemType[] values = EnumSubsystemType.values();
            for (int i = 0; i < values.length-2; i++) {
                if (TardisHelper.getConsoleInWorld(world).isPresent()) {
                    ConsoleTile tile = TardisHelper.getConsoleInWorld(world).get();
                    if (tile != null) {
                        SubsystemItem subsystem = tile.getSubsystem(values[i]).orElse(null);
                          subsystemStates.put(subsystem.getType(), subsystem.isActive());
                    }
                }
            }
        });
        return subsystemStates;
    }
}
