package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.registries.UpgradeRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;

public class EngineUpgradeSliderSyncMessage {
    public int maxSlotSize;
    public Map<Integer, Boolean> upgradeStates = new HashMap<>();
    
    public EngineUpgradeSliderSyncMessage(int maxSlotSize, Map<Integer, Boolean> upgradeStates) {
        this.maxSlotSize = maxSlotSize;
        this.upgradeStates = upgradeStates;
    }
    
    public static void encode(EngineUpgradeSliderSyncMessage mes, PacketBuffer buf) {
        
        buf.writeInt(mes.maxSlotSize);
        buf.writeInt(mes.upgradeStates.size());
        for(Entry<Integer, Boolean> e : mes.upgradeStates.entrySet()) {
            buf.writeInt(e.getKey());
            buf.writeBoolean(e.getValue());
        }
        
    }
    
    public static EngineUpgradeSliderSyncMessage decode(PacketBuffer buf){
        int maxSlotSize = buf.readInt();
        int size = buf.readInt();
        Map<Integer, Boolean> upgradeStates = new HashMap<>();
        for(int i = 0; i < size; ++i) {
            upgradeStates.put(buf.readInt(), buf.readBoolean());
        }
        return new EngineUpgradeSliderSyncMessage(maxSlotSize, upgradeStates);
    }
    
    public static EngineUpgradeSliderSyncMessage create(int maxSlotSize, ServerWorld world) {
        Map<Integer, Boolean> map = getUpgradeStates(maxSlotSize, world);
        return new EngineUpgradeSliderSyncMessage(maxSlotSize, map);
    }
    
    public static void handle(EngineUpgradeSliderSyncMessage mes, Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> {
            if (context.get().getDirection() == NetworkDirection.PLAY_TO_SERVER) {
                Map<Integer, Boolean> map = getUpgradeStates(mes.maxSlotSize, context.get().getSender().getEntityWorld());
                Network.sendTo(new EngineUpgradeSliderSyncMessage(mes.maxSlotSize, map), context.get().getSender());
            }
            else {
                ClientPacketHandler.handleEngineUpgradeSliderSyncClient(mes);
            }
            context.get().setPacketHandled(true);
        });
    }
    
    public static Map<Integer, Boolean> getUpgradeStates(int maxSize, World world){
        Map<Integer, Boolean> upgradeStates = new HashMap<>();
        world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            for (int i = 0; i < maxSize; i++) {
                ItemStack stack = cap.getEngineInventoryForSide(Direction.SOUTH).getStackInSlot(i);
                if (stack != null) {
                    if (!stack.isEmpty()) {
                        UpgradeEntry entry = UpgradeRegistry.getUpgradeFromItem(stack.getItem());
                        if (entry != null) {
                            if (TardisHelper.getConsoleInWorld(world).isPresent()) {
                                ConsoleTile tile = TardisHelper.getConsoleInWorld(world).get();
                                if (tile != null) {
                                    if(tile.getUpgrade(entry.getRegistryName()).isPresent()) {
                                        Upgrade upgrade = tile.getUpgrade(entry.getRegistryName()).get();
                                        upgradeStates.put(i, upgrade.isActivated());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
        return upgradeStates;
    }

    public static Map<Integer, Boolean> getSubsystemStates(int maxSize, World world){
        Map<Integer, Boolean> subsystemStates = new HashMap<>();
        world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            for (int i = 0; i < maxSize; i++) {
                if (TardisHelper.getConsoleInWorld(world).isPresent()) {
                    ConsoleTile tile = TardisHelper.getConsoleInWorld(world).get();
                    EnumSubsystemType[] values = EnumSubsystemType.values();
                    if (tile != null) {
                        SubsystemItem subsystem = tile.getSubsystem(values[i]).orElse(null);
                          subsystemStates.put(i, subsystem.isActive());
                    }
                }
            }
        });
        return subsystemStates;
    }
}
