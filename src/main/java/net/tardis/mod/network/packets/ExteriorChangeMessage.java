package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorChangeMessage {
	
	ResourceLocation exterior;
	
	public ExteriorChangeMessage(ResourceLocation ext) {
		this.exterior = ext;
	}
	
	public static void encode(ExteriorChangeMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.exterior);
	}
	
	public static ExteriorChangeMessage decode(PacketBuffer buf) {
		return new ExteriorChangeMessage(buf.readResourceLocation());
	}
	
	public static void handle(ExteriorChangeMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().getServerWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = (ConsoleTile)te;
				console.getSubsystem(EnumSubsystemType.CHAMELEON).ifPresent(sys -> {
					AbstractExterior ext = ExteriorRegistry.getExterior(mes.exterior);
					if(console.getUnlockManager().getUnlockedExteriors().contains(ext)) {
						if(sys.canBeUsed(console)) {
							sys.damage(context.get().getSender(), 1, console);
							
							boolean hasExt = console.getExteriorType().getExteriorTile(console) != null;
							
							if(hasExt)
								console.getExteriorType().remove(console);
							
							console.setExteriorType(ext);
							if(!console.isInFlight())
								console.getExteriorType().place(console, console.getCurrentDimension(), console.getCurrentLocation());
						}
						else context.get().getSender().sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.NO_COMPONENT, TardisConstants.Translations.CHAMELEON_CIRCUIT.getString()), true);
					}
				});
			}
		});
		context.get().setPacketHandled(true);
	}

}
