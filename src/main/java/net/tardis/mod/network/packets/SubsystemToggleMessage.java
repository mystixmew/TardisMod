package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;

import java.util.function.Supplier;

public class SubsystemToggleMessage {

	private EnumSubsystemType type;
	private boolean isSubsystem;
	boolean activated;

	public SubsystemToggleMessage(EnumSubsystemType type, boolean subsystem, boolean activated) {
		this.type = type;
		this.isSubsystem = subsystem;
		this.activated = activated;
	}

	public static void encode(SubsystemToggleMessage mes, PacketBuffer buf) {
		buf.writeEnumValue(mes.type);
		buf.writeBoolean(mes.isSubsystem);
		buf.writeBoolean(mes.activated);
	}

	public static SubsystemToggleMessage decode(PacketBuffer buf){
		return new SubsystemToggleMessage(buf.readEnumValue(EnumSubsystemType.class), buf.readBoolean(), buf.readBoolean());
	}

	public static void handle(SubsystemToggleMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().getServerWorld()).ifPresent(tile -> {
				if(mes.isSubsystem){
					tile.getSubsystem(mes.type).ifPresent(sys -> {
						sys.setActive(mes.activated);
					});
				}
			});
		});
		context.get().setPacketHandled(true);
	}
}
