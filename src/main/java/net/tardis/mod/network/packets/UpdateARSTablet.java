package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.ARSTabletItem;
import net.tardis.mod.items.TItems;

public class UpdateARSTablet {
	
	private ResourceLocation name;
	
	public UpdateARSTablet(ResourceLocation name) {
		this.name = name;
	}
	
	public static void encode(UpdateARSTablet mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.name);
	}
	
	public static UpdateARSTablet decode(PacketBuffer buf) {
		return new UpdateARSTablet(buf.readResourceLocation());
	}
	
	public static void handle(UpdateARSTablet mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ItemStack stack = context.get().getSender().getHeldItem(PlayerHelper.isInMainHand(context.get().getSender(), TItems.ARS_TABLET.get()) ? Hand.MAIN_HAND : Hand.OFF_HAND);
			if(stack.getItem() == TItems.ARS_TABLET.get()) {
				ARSTabletItem.setPiece(stack, ARSPiece.getRegistry().get(mes.name));
				PlayerHelper.sendMessageToPlayer(context.get().getSender(), new TranslationTextComponent("message.tardis.ars.selected_piece").appendSibling(ARSPiece.getRegistry().get(mes.name).getDisplayName().mergeStyle(TextFormatting.LIGHT_PURPLE)), false);
			}
        });
		context.get().setPacketHandled(true);
	}

}
