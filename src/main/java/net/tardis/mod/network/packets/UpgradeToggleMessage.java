package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.TardisHelper;

public class UpgradeToggleMessage {

	private EnumSubsystemType type;
	private ResourceLocation upgrade;
	private boolean isSubsystem;
	boolean activated;

	public UpgradeToggleMessage(ResourceLocation upgrade, EnumSubsystemType type, boolean subsystem, boolean activated) {
		this.type = type;
		this.upgrade = upgrade;
		this.isSubsystem = subsystem;
		this.activated = activated;
	}

	public static void encode(UpgradeToggleMessage mes, PacketBuffer buf) {
		buf.writeEnumValue(mes.type);
		buf.writeResourceLocation(mes.upgrade);
		buf.writeBoolean(mes.isSubsystem);
		buf.writeBoolean(mes.activated);
	}

	public static UpgradeToggleMessage decode(PacketBuffer buf){
		return new UpgradeToggleMessage(buf.readResourceLocation(), buf.readEnumValue(EnumSubsystemType.class), buf.readBoolean(), buf.readBoolean());
	}

	public static void handle(UpgradeToggleMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().getServerWorld()).ifPresent(tile -> {
				if(!mes.isSubsystem){
					tile.getUpgrade(mes.upgrade).ifPresent(upgrade -> {
						upgrade.setActivated(mes.activated);
						upgrade.update();
					});
				}
			});
		});
		context.get().setPacketHandled(true);
	}
}
