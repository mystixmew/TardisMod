package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.console.misc.DistressSignal;

/**
 * Created by 50ap5ud5
 * on 9 Jul 2020 @ 9:14:31 pm
 */
public class VMDistressMessage {

	private ResourceLocation id;
	private String message;
	
	private static final IFormattableTextComponent DISTRESS_MESSAGE = new TranslationTextComponent("message.vm.distress_signal_message");

	public VMDistressMessage(ResourceLocation id, String message) {
		this.id = id;
		this.message = message;
	}

	public static void encode(VMDistressMessage mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.id);
		buf.writeString(mes.message, TardisConstants.PACKET_STRING_LENGTH);
	}

	public static VMDistressMessage decode(PacketBuffer buf) {
		return new VMDistressMessage(buf.readResourceLocation(), buf.readString(TardisConstants.PACKET_STRING_LENGTH));
	}

	public static void handle(VMDistressMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ServerPlayerEntity player = context.get().getSender();
			ServerWorld world = player.getServerWorld();
			TardisHelper.getConsole(player.getServer(), mes.id).ifPresent(tile -> {
				tile.getSubsystem(EnumSubsystemType.ANTENNA).ifPresent(sys -> {
					if(sys.canBeUsed(tile)) {
						tile.addDistressSignal(new DistressSignal(mes.message.isEmpty() ? DISTRESS_MESSAGE.getString(): mes.message, new SpaceTimeCoord(world.getDimensionKey(), player.getPosition())));
						tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
							PlayerHelper.sendMessageToPlayer(player, TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.distress_sent", data.getTARDISName())), false);
						});
						player.getCooldownTracker().setCooldown(TItems.VORTEX_MANIP.get(), TConfig.SERVER.vmCooldownTime.get() * 20);
					}
			    });
			});
		});
		context.get().setPacketHandled(true);
	}
}
