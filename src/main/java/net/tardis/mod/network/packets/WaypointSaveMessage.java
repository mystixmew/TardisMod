package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointSaveMessage {

	private static final String SUCCESS = "message.tardis.waypoint.saved";
	private static final String FAIL = "message.tardis.waypoint.save_fail";
	private static final String FAIL_DIM = "message.tardis.waypoint.save_fail_dimension";
	private String name;
	
	public WaypointSaveMessage(String name) {
		this.name = name;
	}
	
	public static void encode(WaypointSaveMessage mes, PacketBuffer buf) {
		buf.writeString(mes.name, TardisConstants.PACKET_STRING_LENGTH);
	}
	
	public static WaypointSaveMessage decode(PacketBuffer buf) {
		return new WaypointSaveMessage(buf.readString(TardisConstants.PACKET_STRING_LENGTH));
	}
	
	public static void handle(WaypointSaveMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			
			TardisHelper.getConsoleInWorld(cont.get().getSender().getServerWorld()).ifPresent(console -> {
				
				if(WorldHelper.canTravelToDimension(cont.get().getSender().getServer().getWorld(console.getCurrentDimension()))) {
					boolean success = false;
					
					for(TileEntity te : cont.get().getSender().getServerWorld().loadedTileEntityList) {
						
						SpaceTimeCoord coord = new SpaceTimeCoord(console.getCurrentDimension(), console.getCurrentLocation(), console.getTrueExteriorFacingDirection());
						coord.setName(mes.name);
						
						if(te instanceof WaypointBankTile) {
							success = ((WaypointBankTile)te).addWaypoint(coord);
							if(success) {
								cont.get().getSender().sendStatusMessage(new TranslationTextComponent(SUCCESS, mes.name), true);
								break;
							}
						}
					}
					
					if(!success)
						cont.get().getSender().sendStatusMessage(new TranslationTextComponent(FAIL, mes.name), true);
				}
				else cont.get().getSender().sendStatusMessage(new TranslationTextComponent(FAIL_DIM, mes.name), true);
				
			});
			
		});
		cont.get().setPacketHandled(true);
	}
}
