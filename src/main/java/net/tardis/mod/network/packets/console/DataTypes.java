package net.tardis.mod.network.packets.console;

import java.util.HashMap;
import java.util.Map;

import net.tardis.mod.helper.Helper;

public class DataTypes {
	
	public static Map<String, DataType> CONSOLE_DATA_TYPES = new HashMap<String, DataType>();
	
	public static final DataType LAND_CODE = addDataType("land_code", new DataType(() -> new LandCode("")));
    public static final DataType FUEL = addDataType("fuel" , new DataType(() -> new Fuel(0, 0)));
    public static final DataType SOUND_SCHEME = addDataType("sound_scheme" , new DataType(() -> new SoundSchemeData()));
    public static final DataType FORCEFIELD = addDataType("forcefield", new DataType(() -> new ForcefieldData(false)));
    public static final DataType ANTIGRAVS = addDataType("anti_gravs" , new DataType(() -> new AntigravsData(false)));
    public static final DataType CRASH = addDataType("crash" , new DataType(() -> new CrashData()));
    public static final DataType SUBSYSTEM = addDataType("subsystem" , new DataType(() -> new SubsystemData(null, false, false)));
    public static final DataType NAV_COM = addDataType("nav_com" , new DataType(() -> new NavComData(false)));
    public static final DataType SHIELD = addDataType("shield", new DataType(() -> new ShieldData(false)));
    public static final DataType DIMENSION_LIST = addDataType("dim_list" , new DataType(() -> new DimensionData(0, 0)));
    public static final DataType UPGRADE = addDataType("upgrade" , new DataType(() -> new UpgradeData(null, true)));
    public static final DataType UNLOCK = addDataType("unlock" , new DataType(() -> new UnlockData(null)));
	
    private static DataType addDataType(String id, DataType type) {
    	String regId = Helper.createRLString(id);
    	DataType dataType = type.setRegistryId(regId);
    	CONSOLE_DATA_TYPES.put(regId, dataType);
    	return dataType;
    }
}
