package net.tardis.mod.particles;

import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TParticleTypes {

	public static final DeferredRegister<ParticleType<?>> TYPES = DeferredRegister.create(ForgeRegistries.PARTICLE_TYPES, Tardis.MODID);
	
	public static final RegistryObject<BasicParticleType> ARTRON = TYPES.register("artron", () -> new BasicParticleType(false));
	public static final RegistryObject<BasicParticleType> BUBBLE = TYPES.register("bubble", () -> new BasicParticleType(false));
}
