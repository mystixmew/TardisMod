package net.tardis.mod.registries;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.stats.IStatFormatter;
import net.minecraft.stats.Stats;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.tardis.mod.Tardis;

public class TardisStatistics {
	
	private static Map<ResourceLocation, IStatFormatter> CUSTOM_STAT_ENTRIES = new HashMap<>();

	public static final ResourceLocation SUBSYSTEMS_BROKEN = registerCustom("subsystems_broken", IStatFormatter.DEFAULT);
	public static final ResourceLocation TARDIS_DISTANCE_TRAVELLED = registerCustom("tardis_distance_travelled", IStatFormatter.DISTANCE);
	public static final ResourceLocation TARDIS_CRASH_COUNT = registerCustom("tardis_crash_count", IStatFormatter.DEFAULT);
	public static final ResourceLocation FLIGHT_EVENTS_COMPLETED = registerCustom("flight_events_completed", IStatFormatter.DEFAULT);
	public static final ResourceLocation VORTEX_TRAVEL_COUNT = registerCustom("vortex_travel_count", IStatFormatter.DEFAULT);
	public static final ResourceLocation MISSIONS_COMPLETED = registerCustom("missions_completed", IStatFormatter.DEFAULT);
	
	private static ResourceLocation registerCustom(String key, IStatFormatter formatter) {
		ResourceLocation resourcelocation = new ResourceLocation(Tardis.MODID, key); 
		CUSTOM_STAT_ENTRIES.put(resourcelocation, formatter);
	    return resourcelocation;
    }
	
	/** Adds the Stat to Vanilla Custom Stat registry and makes a Stat in the custom stat's map
	 * <br> Called during FMLCommonSetup*/
	public static void addStatTypesToVanilla() {
		CUSTOM_STAT_ENTRIES.forEach((resourcelocation, formatter) -> {
			Registry.register(Registry.CUSTOM_STAT, resourcelocation, resourcelocation); //There is no Forge registry for the Custom stat so we use vanilla's
			Stats.CUSTOM.get(resourcelocation, formatter); //Adds our stat to the stat type's map
		});	
	}
}
