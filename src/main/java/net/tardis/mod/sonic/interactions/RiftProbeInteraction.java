package net.tardis.mod.sonic.interactions;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.chunk.Chunk;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.items.TItems;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sounds.TSounds;

public class RiftProbeInteraction extends AbstractSonicMode {

    public static final String RIFT = "message." + Tardis.MODID + ".found_rift";

    @Override
    public void inventoryTick(PlayerEntity player, ItemStack sonic) {
        //Poll every second
        if(player.ticksExisted % 20 == 0){
            Chunk c = player.world.getChunkAt(player.getPosition());
            c.getCapability(Capabilities.RIFT).ifPresent(rift ->{
                if(rift.isRift())
                    onFoundRift(player, rift.getRiftEnergy());
            });
        }
    }

    public void onFoundRift(PlayerEntity player, float riftEnergy){
        if(!player.world.isRemote) {
            player.sendStatusMessage(new TranslationTextComponent(RIFT, riftEnergy).mergeStyle(TextFormatting.LIGHT_PURPLE), true);
        }
        if (TConfig.CLIENT.playToolNotificationSounds.get())
            player.playSound(TSounds.REMOTE_ACCEPT.get(), SoundCategory.PLAYERS, 0.5F, 1F);
    }

	@Override
	public ItemStack getItemDisplayIcon() {
		return new ItemStack(TItems.DIAGNOSTIC_TOOL.get());
	}
    
    
}
