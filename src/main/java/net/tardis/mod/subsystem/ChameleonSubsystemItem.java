package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class ChameleonSubsystemItem extends SubsystemItem implements IChameleonSubsystem {
    public ChameleonSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public boolean stopsFlight(ConsoleTile console) {
        return false;
    }

    @Override
    public void onTakeoff(ConsoleTile console) {}

    @Override
    public void onLand(ConsoleTile console) {
        this.damage(null, 1, console);
        if (console.hasWorld() && console.getWorld().rand.nextDouble() < 0.05)
            this.damage((ServerPlayerEntity)console.getPilot(), (int) (console.getWorld().rand.nextDouble() * 50), console);
    }

    @Override
    public void onFlightSecond(ConsoleTile console) {}

    @Override
    public SparkingLevel getSparkState() {
        return SparkingLevel.NONE;
    }
    /**
     * If this exterior is currently the disguise exterior
     * @param checkDamage - if we should account for the durability of the item
     * @return
     */
    @Override
    public boolean isActiveCamo(boolean checkDamage, ConsoleTile console) {
        if(console.getExteriorType() != ExteriorRegistry.DISGUISE.get())
            return false;
        return checkDamage ? this.canBeUsed(console) : true;

    }
}
