package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.tileentities.ConsoleTile;

public class FlightSubsystemItem extends SubsystemItem {

    int flightSecs = 0;

    public FlightSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public void onTakeoff(ConsoleTile console) {
        this.flightSecs = 0;
    }

    @Override
    public void onLand(ConsoleTile console) {
        this.flightSecs = 0;
    }

    @Override
    public void onFlightSecond(ConsoleTile console) {
        if (flightSecs % 3 == 0)
            this.damage((ServerPlayerEntity)console.getPilot(), 1, console);
        ++this.flightSecs;
    }
}
