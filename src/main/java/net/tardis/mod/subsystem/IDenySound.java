package net.tardis.mod.subsystem;

import net.tardis.mod.tileentities.ConsoleTile;

public interface IDenySound {
    void playDenySound(ConsoleTile console);
}
