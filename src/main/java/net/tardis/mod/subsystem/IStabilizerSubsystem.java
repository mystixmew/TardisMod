package net.tardis.mod.subsystem;

import net.tardis.mod.tileentities.ConsoleTile;

public interface IStabilizerSubsystem {
    boolean isControlActivated(ConsoleTile console);

    void setControlActivated(boolean b, ConsoleTile console);
}
