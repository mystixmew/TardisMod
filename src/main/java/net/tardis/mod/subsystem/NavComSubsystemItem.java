package net.tardis.mod.subsystem;

import net.minecraft.util.SoundCategory;
import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class NavComSubsystemItem extends SubsystemItem implements IDenySound {
    public NavComSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }

    @Override
    public SparkingLevel getSparkState() {
        return SparkingLevel.NONE;
    }

    @Override
    public void playDenySound(ConsoleTile console) {
        if(console.getWorld() != null)
            console.getWorld().playSound(null, console.getPos(), TSounds.SONIC_FAIL.get(), SoundCategory.BLOCKS, 1.0F, 1.0F);

    }
}
