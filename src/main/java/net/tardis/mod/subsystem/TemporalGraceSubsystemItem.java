package net.tardis.mod.subsystem;

import net.tardis.mod.enums.EnumSubsystemType;
import net.tardis.mod.items.SubsystemItem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;

public class TemporalGraceSubsystemItem extends SubsystemItem {
    public TemporalGraceSubsystemItem(Properties prop, EnumSubsystemType type, boolean required, boolean repairable) {
        super(prop, type, required, repairable);
    }
    @Override
    public boolean stopsFlight(ConsoleTile console) {
        return false;
    }

    @Override
    public SparkingLevel getSparkState() {
        return SparkingLevel.NONE;
    }
}
