package net.tardis.mod.tags;

import net.minecraft.item.Item;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.helper.Helper;

public class TardisItemTags {

    public static final ITag<Item> CINNABAR = makeItem(new ResourceLocation("forge", "dusts/cinnabar"));
    public static final ITag<Item> PLANTS = makeItem(Helper.createRL("plants"));
    public static final ITag<Item> ARS = makeItem(Helper.createRL("ars"));

    public static final ITag.INamedTag<Item> CORAL = makeItem(new ResourceLocation("forge", "coral"));
    public static final ITag.INamedTag<Item> DEAD_CORAL = makeItem(new ResourceLocation("forge", "dead_coral"));
    public static final ITag.INamedTag<Item> CONCRETE = makeItem(new ResourceLocation("forge", "concrete"));

    public static ITag.INamedTag<Item> makeItem(ResourceLocation resourceLocation) {
        return ItemTags.makeWrapperTag(resourceLocation.toString()); //makeWrapperTag can be static inited and is aware of tag reloads. Do not use createOptional because that gets loaded too early.
    }
}
