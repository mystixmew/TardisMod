package net.tardis.mod.tileentities.console.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.tileentities.ConsoleTile;
/** Wrapper class for segmenting artron fuel usage
 * <br> Allows us to set aside a certain amount of artron for different tasks and be able to track them
 * <br> Note: The use per tick of the artron use
 * @author Spectre0987*/
public class ArtronUse {
	
	private IArtronType type;
	private float usePerTick = 0F;
	private int ticksToDrain = 0;
	
	protected ArtronUse() {}
	
	public ArtronUse(IArtronType type) {
		this.type = type;
		this.usePerTick = type.getUse();
	}
	
	public void setArtronUsePerTick(float usePerTick) {
		this.usePerTick = usePerTick;
	}
	
	public float getArtronUsePerTick() {
		return this.usePerTick;
	}
	
	public boolean isActive() {
		return this.ticksToDrain > -1;
	}
	
	public void setTicksToDrain(int time) {
		this.ticksToDrain = time;
	}
	
	public void tick(ConsoleTile tile) {
		if(this.ticksToDrain >= 0) {
			
			if(this.ticksToDrain > 0) {
				tile.setArtron(tile.getArtron() - this.getArtronUsePerTick());
			}
			
			--this.ticksToDrain;
		}
	}
	
	public IArtronType getType() {
		return this.type;
	}
	
	public CompoundNBT serialiseNBT() {
	    CompoundNBT nbt = new CompoundNBT();
	    nbt.putString("id", this.type.getId());
	    nbt.putFloat("use_per_tick", this.usePerTick);
	    nbt.putInt("ticks", this.ticksToDrain);
	    return nbt;
	}
	
	public static ArtronUse deserialiseNBT(CompoundNBT nbt) {
		ArtronType type = ArtronType.valueOf(nbt.getString("id").toUpperCase());
	    float artronPerTick = nbt.getFloat("use_per_tick");
	    int ticks = nbt.getInt("ticks");
	    if (type == null)
	    	type = ArtronType.DUMMY;
	    ArtronUse use = new ArtronUse(type);
	    use.setArtronUsePerTick(artronPerTick);
	    use.setTicksToDrain(ticks);
	    return use;
	}
	
	public static interface IArtronType{
		float getUse();
		TranslationTextComponent getTranslation();
		String getId();
	}

	public static enum ArtronType implements IArtronType{
		FLIGHT(0F, "flight"),
		FORCEFIELD(0.2F, "forcefield"),
		CONVERTER(1.0F, "converter"),
		ANTIGRAVS(0.2F, "antigravs"),
		INTERIOR_CHANGE(1.0F, "interior_change"),
		DUMMY(1.0F, "dummy");

		float use;
		TranslationTextComponent trans;
		String id;
		
		ArtronType(float use, String name){
			this.use = use;
			trans = new TranslationTextComponent("artronuse.tardis." + name);
			this.id = name;
		}
		
		@Override
		public float getUse() {
			return this.use;
		}

		@Override
		public TranslationTextComponent getTranslation() {
			return this.trans;
		}
		
		@Override
		public String getId() {
		    return this.id;
		}
		
	}
}
