package net.tardis.mod.tileentities.exteriors;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class TTCapsuleExteriorTile extends ExteriorTile{

	public static final AxisAlignedBB NORTH_DOOR = new AxisAlignedBB(0, -0.8, -0.1, 1, 1.15, 0.5);
	
	public TTCapsuleExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TTCapsuleExteriorTile() {
		super(TTiles.EXTERIOR_TT_CAPSULE.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(world != null && world.getBlockState(getPos()).hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			switch(world.getBlockState(this.getPos()).get(BlockStateProperties.HORIZONTAL_FACING)) {
				case EAST: return new AxisAlignedBB(0.5, -0.8, 0, 1.1, 1.15, 1);
				case SOUTH: return new AxisAlignedBB(0, -1, 0.5, 1, 1, 1.1);
				case WEST: return new AxisAlignedBB(-0.1, -1, 0, 0.5, 1, 1);
				default: return NORTH_DOOR;
			}
		}
		return new AxisAlignedBB(0, 0, 0, 0, 0, 0);
		
	}

}
