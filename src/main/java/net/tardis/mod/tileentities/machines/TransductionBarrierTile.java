package net.tardis.mod.tileentities.machines;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockState;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.tardis.mod.blocks.TransductionBarrierBlock;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.entity.TardisDisplayEntity;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.IAffectTARDISLanding;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.tileentities.console.misc.AlarmType;
import net.tardis.mod.tileentities.console.misc.MonitorOverride;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TransductionBarrierTile extends TileEntity implements ITickableTileEntity, IAffectTARDISLanding {

	private String landingCode = "";
	
	private EnergyStorage power = getEnergyCap();
	private LazyOptional<EnergyStorage> powerHolder = LazyOptional.of(() -> this.power);
	
	public TransductionBarrierTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TransductionBarrierTile() {
		this(TTiles.TRANSDUCTION_BARRIER.get());
	}
	
	public void setCode(String code) {
		this.landingCode = code;
		this.markDirty();
	}
	
	public String getCode() {
		return this.landingCode;
	}
	
	public boolean canLand(ConsoleTile tile) {
		return this.power.getEnergyStored() >= TConfig.SERVER.transductionDrainAmount.get() &&
				tile.getLandingCode().toLowerCase().contentEquals(landingCode.toLowerCase());
	}
	
	public void onBlockedTARDIS(ConsoleTile tile) {
		this.power.extractEnergy(TConfig.SERVER.transductionDrainAmount.get(), false);
		this.markDirty();
	}

	@Override
	public void read(BlockState state, CompoundNBT compound) {
		super.read(state, compound);
		this.landingCode = compound.getString("landing_code");
		this.power.receiveEnergy(compound.getInt("power"), false);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putString("landing_code", this.landingCode);
		compound.putInt("power", this.power.getEnergyStored());
		return super.write(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		
		if(cap == CapabilityEnergy.ENERGY)
			return powerHolder.cast();
		
		return super.getCapability(cap, side);
	}

	@Override
	public void tick() {
		if(!world.isRemote && power.getEnergyStored() > 0 && world.getGameTime()+ 5 % 20 == 0) {
			if (this.getBlockState().hasProperty(TransductionBarrierBlock.ACTIVATED) && this.getBlockState().get(TransductionBarrierBlock.ACTIVATED) == true) {
			    this.power.extractEnergy(1, false);
			}
		}
	}

	@Override
	public SpaceTimeCoord affectTARDIS(ServerWorld world, SpaceTimeCoord currentLanding, ConsoleTile console) {
		if(!this.canLand(console)) {
			if (this.getBlockState().hasProperty(TransductionBarrierBlock.ACTIVATED) && this.getBlockState().get(TransductionBarrierBlock.ACTIVATED) == true) {
				Random rand = world.rand;
				BlockPos landSpot = LandingSystem.getLand(world, pos.offset(Direction.byHorizontalIndex(rand.nextInt(4)), 64), LandingTypeControl.EnumLandType.DOWN, console);
				console.getInteriorManager().soundAlarm(AlarmType.LOW);
				this.onBlockedTARDIS(console);

				//build monitor message
				List<String> list = new ArrayList<>();
				list.add(new TranslationTextComponent("text.tardis.transduction.line1").getString());
				list.add(new TranslationTextComponent("text.tardis.transduction.line2").getString());
				list.add(new TranslationTextComponent("text.tardis.transduction.line3").getString());
				console.getInteriorManager().setMonitorOverrides(new MonitorOverride(console, 100, list));
				
				TardisDisplayEntity entity = new TardisDisplayEntity(world);
				BlockPos originalLandPos = currentLanding.getPos();
				//1.17: createBlockEntity
				TileEntity ext = console.getExteriorType().getDefaultState().createTileEntity(world);
				if(ext != null)
					entity.setTile((ExteriorTile)ext);
				entity.setPosition(originalLandPos.getX() + 0.5, originalLandPos.getY() + 1, originalLandPos.getZ() + 0.5);
				entity.setMotion((rand.nextDouble() - 0.5) * 0.5, rand.nextDouble() * 0.5, (rand.nextDouble() - 0.5) * 0.5);
				world.addEntity(entity);
				
				return new SpaceTimeCoord(world.getDimensionKey(), landSpot);
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}

	@Override
	public int getEffectiveRange() {
		return TConfig.SERVER.transductionEffectiveRange.get();
	}
	
	public EnergyStorage getEnergyCap() {
		return new EnergyStorage(TConfig.SERVER.transductionEnergyMaxCapacity.get());
	}
}
