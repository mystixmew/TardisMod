package net.tardis.mod.traits;

import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.RainType;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ColdBloodedTrait extends TardisTrait{

	public static final TranslationTextComponent TOO_COLD = new TranslationTextComponent(createTranslationKey(false, Helper.createRL("coldblood")));
	LazyOptional<ExteriorTile> exteriorHolder;

	public ColdBloodedTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		
		if(!tile.isInFlight() && tile.getWorld().getGameTime() % 20 == 0) {

			if(this.exteriorHolder == null || !this.exteriorHolder.isPresent())
				this.exteriorHolder = tile.getOrFindExteriorTile();
			//Lose mood if in a cold biome.
			this.exteriorHolder.ifPresent(ext -> {
				Biome b = ext.getWorld().getBiome(ext.getPos());
				if(((b.getPrecipitation() == RainType.SNOW || b.getTemperature(ext.getPos()) <= 0.15F) && tile.getEmotionHandler().getMood() > EnumHappyState.DISCONTENT.getTreshold())) {
					tile.getEmotionHandler().addMood(-(0.05 + (this.getModifier() * 0.20)));
					this.warnPlayerLooped(tile, TOO_COLD, 400);
				}
			});
		}
		
	}

}
