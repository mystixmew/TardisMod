package net.tardis.mod.world.structures;

import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.common.collect.ImmutableMap;

import net.minecraft.entity.SpawnReason;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Mirror;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.world.IServerWorld;
import net.minecraft.world.gen.feature.structure.StructurePiece;
import net.minecraft.world.gen.feature.structure.TemplateStructurePiece;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.TemplateManager;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.loottables.TardisLootTables;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.registries.MissionRegistry;
import net.tardis.mod.schematics.Schematics;

public class SpaceStationStructurePieces {
    
    private static final ResourceLocation SPACE_STATION_WITH_DRONE = new ResourceLocation(Tardis.MODID, "space/spacestation_drone");
    private static final ResourceLocation[] ALL_STRUCTURES = new ResourceLocation[]{SPACE_STATION_WITH_DRONE};
    
    private static final Map<ResourceLocation, BlockPos> OFFSET = ImmutableMap.of(SPACE_STATION_WITH_DRONE, BlockPos.ZERO);
    
    
    public static void start(TemplateManager templateManager, BlockPos pos, Rotation rotation, List<StructurePiece> pieceList, Random random) {
        int x = pos.getX();
        int z = pos.getZ();
        BlockPos rotationOffSet = new BlockPos(0, 0, 0).rotate(rotation);
        BlockPos blockpos = rotationOffSet.add(x, pos.getY(), z);
        pieceList.add(new SpaceStationStructurePieces.Piece(templateManager, ALL_STRUCTURES[random.nextInt(ALL_STRUCTURES.length)], blockpos, rotation)); //Hardcode this for now to prevent out of bounds error
    }
    
    public static class Piece extends TemplateStructurePiece {
        private final ResourceLocation resourceLocation;
        private final Rotation rotation;

        public Piece(TemplateManager templateManagerIn, ResourceLocation resourceLocationIn, BlockPos pos, Rotation rotationIn) {
            super(TStructures.Structures.SPACE_STATION_DRONE_PIECE, 0);
            this.resourceLocation = resourceLocationIn;
            BlockPos blockpos = SpaceStationStructurePieces.OFFSET.get(resourceLocation);
            this.templatePosition = pos.add(blockpos.getX(), blockpos.getY(), blockpos.getZ());
            this.rotation = rotationIn;
            this.setupPiece(templateManagerIn);
        }
        
        public Piece(TemplateManager templateManagerIn, CompoundNBT tagCompound) {
            super(TStructures.Structures.SPACE_STATION_DRONE_PIECE, tagCompound);
            this.resourceLocation = new ResourceLocation(tagCompound.getString("Template"));
            this.rotation = Rotation.valueOf(tagCompound.getString("Rot"));
            this.setupPiece(templateManagerIn);
        }
        
        private void setupPiece(TemplateManager templateManager) {
            Template template = templateManager.getTemplateDefaulted(this.resourceLocation);
            PlacementSettings placementsettings = (new PlacementSettings()).setRotation(this.rotation).setMirror(Mirror.NONE);
            this.setup(template, this.templatePosition, placementsettings);
        }
        
        /**
         * (abstract) Helper method to read subclass data from NBT
         */
        @Override
        protected void readAdditional(CompoundNBT tagCompound) {
            super.readAdditional(tagCompound);
            tagCompound.putString("Template", this.resourceLocation.toString());
            tagCompound.putString("Rot", this.rotation.name());
        }

        @Override
        protected void handleDataMarker(String function, BlockPos pos, IServerWorld worldIn, Random rand, MutableBoundingBox sbb) {
            if ("captain_spawn".equals(function)) {
                final BlockPos entCap = pos.toImmutable();
                ShipCaptainEntity cap = TEntities.SHIP_CAPTAIN.get().create(worldIn.getWorld());
                cap.setPosition(entCap.getX() + 0.5, entCap.getY() + 1, entCap.getZ() + 0.5);
                cap.onInitialSpawn(worldIn.getWorld(), worldIn.getDifficultyForLocation(entCap), SpawnReason.STRUCTURE, null, null);
                worldIn.addEntity(cap);
                worldIn.removeBlock(pos, false);
            }
            if ("mission_marker".equals(function)) {
                ServerWorld serverWorld = worldIn.getWorld();
                serverWorld.getServer().enqueue(new TickDelayedTask(0, () -> {
                    serverWorld.getCapability(Capabilities.MISSION).ifPresent(missions -> {
                        MiniMission mis = MissionRegistry.STATION_DRONE.get().create(serverWorld.getWorldServer(), pos, 64);
                        missions.addMission(mis);
                        Network.sendToAllInWorld(new MissionUpdateMessage(mis), worldIn.getWorld());
                    });
                }));
                worldIn.removeBlock(pos, false);
            }
            if ("crewmates".equals(function)) {
                int num = 5 + worldIn.getRandom().nextInt(3);
                for (int i = 0; i < num; ++i) {
                    CrewmateEntity entity = TEntities.CREWMATE.get().create(worldIn.getWorld());
                    entity.setPosition(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
                    entity.onInitialSpawn(worldIn.getWorld(), worldIn.getDifficultyForLocation(pos), SpawnReason.STRUCTURE, null, null);
                    worldIn.addEntity(entity);
                }
                worldIn.removeBlock(pos, false);
            }
            if ("tardis:spacestation_lab_loot_1".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.SPACESTATION_DRONE);
            	Helper.addSchematicToComputerBelow(worldIn, pos, Schematics.Exteriors.TT_2020.getId());
            }
            if ("tardis:spacestation_central_loot_1".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.SPACESTATION_DRONE);
            }
            if ("tardis:spacestation_central_loot_2".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.SPACESTATION_DRONE);
            }
            if ("tardis:spacestation_central_loot_3".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.CRASHED_SHIP);
            }
            if ("tardis:spacestation_central_loot_4".equals(function)) {
            	Helper.addLootToComputerBelow(worldIn, pos, TardisLootTables.SPACESTATION_DRONE);
            }
        }
        
    }
}
