package net.tardis.mod.world.surfacebuilders;

import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.surfacebuilders.ConfiguredSurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.ISurfaceBuilderConfig;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilder;
import net.minecraft.world.gen.surfacebuilders.SurfaceBuilderConfig;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

public class TSurfaceBuilders {
	
	
	public static class SurfaceBuilders{
		 public static final DeferredRegister<SurfaceBuilder<?>> SURFACE_BUILDERS = DeferredRegister.create(ForgeRegistries.SURFACE_BUILDERS, Tardis.MODID);
		 
		 //Register the earlier created instance of our surface builder. We have to do this seperately the surface builders register AFTER biomes, so this is just to ensure it won't cause issues when the game has fully setup.
		 public static final RegistryObject<SurfaceBuilder<SurfaceBuilderConfig>> MOON_SURFACE_BUILDER = createSurfaceBuilder("moon_surface_builder", () -> new MoonSurfaceBuilder(SurfaceBuilderConfig.CODEC));
    
	}
    
    private static <S extends SurfaceBuilder<?>> RegistryObject<S> createSurfaceBuilder(String name, Supplier<? extends S> surfaceBuilder) {
        return SurfaceBuilders.SURFACE_BUILDERS.register(name, surfaceBuilder);
    }
    
    private static <SC extends ISurfaceBuilderConfig> ConfiguredSurfaceBuilder<SC> registerConfiguredSurfaceBuilder(String name, ConfiguredSurfaceBuilder<SC> configuredSurfaceBuilder) {
        return WorldGenRegistries.register(WorldGenRegistries.CONFIGURED_SURFACE_BUILDER, new ResourceLocation(Tardis.MODID, name), configuredSurfaceBuilder);
    }
    
   
}
